﻿/*
					Copyright 2017 c0dycode

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace BL2HexMultitool
{
    public class CheckPatched
    {
        #if DEBUG
        public const string BLPath = "E:\\SteamLibrary\\steamapps\\common\\Borderlands 2\\Binaries\\Win32\\Borderlands2.exe";
        #else
        public const string BLPath = "Borderlands2.exe";
        #endif

        public static int ResultedAddress { get; set; }

        /// <summary>
        /// Scanning the File for the supplied pattern and if supplied, add/subtract given offset
        /// </summary>
        /// <param name="pattern">The byte-Pattern to search for in the file</param>
        /// <param name="offset">The offset that should be added/removed to/from the result before returning the address</param>
        /// <returns></returns>
        public static int ScanForPatternAddress(byte[] pattern, int offset = 0)
        {
            try
            {
                byte[] toBeSearched = File.ReadAllBytes(BLPath);

                List<int> positions = SearchPattern.SearchBytePattern(pattern, toBeSearched);
                

                foreach (var item in positions)
                {
                    ResultedAddress = item;
                }
                

                if(Math.Sign(offset) == 1 || Math.Sign(offset) == 0)
                    ResultedAddress = ResultedAddress + Math.Abs(offset);
                else
                    ResultedAddress = ResultedAddress - Math.Abs(offset);
                
                return ResultedAddress;
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("Could not find Borderlands2.exe!");
                return 1;
            }
            catch (IOException excep)
            {
                MessageBox.Show("Could not open Borderlands2.exe. Is it opened by another application?");
                return 1;
            }
            catch (Exception excep)
            {
                MessageBox.Show("Something went wrong!\n{0}", excep.Message);
                return 1;
            }
        }

        public static int CheckMaxLvL1(byte[] pattern)
        {
            try
            {
                byte[] toBeSearched = File.ReadAllBytes(BLPath);

                List<int> positions = SearchPattern.SearchBytePattern(pattern, toBeSearched);

                int addresslvlv1 = 0;
                foreach (var item in positions)
                {
                    addresslvlv1 = item;
                    //MessageBox.Show("Pattern matched at pos " + item);
                }
                addresslvlv1 = addresslvlv1 - 1;
                string hexaddresslvlv1 = addresslvlv1.ToString("X");
                Properties.Settings.Default.addresslvlv1 = addresslvlv1;
                Properties.Settings.Default.hexaddresslvlv1 = hexaddresslvlv1;
                Properties.Settings.Default.Save();
                //MessageBox.Show("First Check Hex: 0x" + hexaddressv1 + "   " + addressv1);
                return addresslvlv1;
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("Could not find Borderlands2.exe!");
                return 1;
            }

        }

        public static int CheckMaxLvL2(byte[] pattern)
        {
            try
            {
                byte[] toBeSearched = File.ReadAllBytes(BLPath);

                List<int> positions = SearchPattern.SearchBytePattern(pattern, toBeSearched);

                int addresslvlv2 = 0;
                foreach (var item in positions)
                {
                    addresslvlv2 = item;
                    //MessageBox.Show("Pattern matched at pos " + item);
                }
                addresslvlv2 = addresslvlv2 + 11;
                string hexaddresslvlv2 = addresslvlv2.ToString("X");
                Properties.Settings.Default.addresslvlv2 = addresslvlv2;
                Properties.Settings.Default.hexaddresslvlv2 = hexaddresslvlv2;
                Properties.Settings.Default.Save();
                //MessageBox.Show("0x" + hexaddresslvlv2 + "   " + addresslvlv2);
                return addresslvlv2;
            }

            catch (FileNotFoundException)
            {
                MessageBox.Show("Could not find Borderlands2.exe!");
                return 1;
            }
            //return 1;
        }
        public static int CheckLVLScaling(byte[] pattern)
        {
            try
            {
                byte[] toBeSearched = File.ReadAllBytes(BLPath);

                List<int> positions = SearchPattern.SearchBytePattern(pattern, toBeSearched);

                int lvlscalingaddress = 0;
                foreach (var item in positions)
                {
                    lvlscalingaddress = item;
                    //MessageBox.Show("Pattern matched at pos " + item);
                }
                lvlscalingaddress = lvlscalingaddress - 5;
                string hexlvlscalingaddress = lvlscalingaddress.ToString("X");
                Properties.Settings.Default.lvlscalingaddress = lvlscalingaddress;
                Properties.Settings.Default.hexlvlscalingaddress = hexlvlscalingaddress;
                Properties.Settings.Default.Save();
                //MessageBox.Show("Hex: 0x" + hexlvlscalingaddress + "   " + lvlscalingaddress);
                return lvlscalingaddress;
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("Could not find Borderlands2.exe!");
                return 1;
                //Application.Exit();
            }
            //return 1;
        }
        public static int CheckEnemyLVLScaling(byte[] pattern)
        {
            try
            {
                byte[] toBeSearched = File.ReadAllBytes(BLPath);

                List<int> positions = SearchPattern.SearchBytePattern(pattern, toBeSearched);

                int enemylvlscalingaddress = 0;
                foreach (var item in positions)
                {
                    enemylvlscalingaddress = item;
                    //MessageBox.Show("Pattern matched at pos " + item);
                }
                enemylvlscalingaddress = enemylvlscalingaddress + 11;
                string hexenemylvlscalingaddress = enemylvlscalingaddress.ToString("X");
                Properties.Settings.Default.enemylvlscalingaddress = enemylvlscalingaddress;
                Properties.Settings.Default.hexenemylvlscalingaddress = hexenemylvlscalingaddress;
                Properties.Settings.Default.Save();
                //MessageBox.Show("Hex: 0x" + hexenemylvlscalingaddress + "   " + enemylvlscalingaddress);
                return enemylvlscalingaddress;
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("Could not find Borderlands2.exe!");
                return 1;
                //Application.Exit();
            }
            //return 1;
        }
        public static int CheckMaxErid(byte[] pattern)
        {
            try
            {
                byte[] toBeSearched = File.ReadAllBytes(BLPath);

                List<int> positions = SearchPattern.SearchBytePattern(pattern, toBeSearched);

                int eridaddress = 0;
                foreach (var item in positions)
                {
                    eridaddress = item;
                    //MessageBox.Show("Pattern matched at pos " + item);
                }
                eridaddress = eridaddress + 16;
                string eridhexaddress = eridaddress.ToString("X");
                Properties.Settings.Default.eridaddress = eridaddress;
                Properties.Settings.Default.eridhexaddress = eridhexaddress;
                Properties.Settings.Default.Save();
                //MessageBox.Show("Hex: 0x" + eridhexaddress + "   " + eridaddress);
                return eridaddress;
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("Could not find Borderlands2.exe!");
                return 1;
                //Application.Exit();
            }
            //return 1;
        }

        public static int CheckPatch(byte[] pattern)
        {
            byte[] toBeSearched = File.ReadAllBytes(BLPath);

            List<int> positions = SearchPattern.SearchBytePattern(pattern, toBeSearched);

            int address = 0;
            foreach (var item in positions)
            {
                address = item;
                //MessageBox.Show("Pattern matched at pos " + item);
            }
            address = address + 2;
            string hexaddress = address.ToString("X");
            Properties.Settings.Default.address = address;
            Properties.Settings.Default.hexaddress = hexaddress;
            Properties.Settings.Default.Save();
            //MessageBox.Show("Hex: 0x" + hexaddress + "   " + address);
            return address;
        }
        public static int CheckTestPatch(byte[] pattern)
        {
            byte[] toBeSearched = File.ReadAllBytes(BLPath);

            List<int> positions = SearchPattern.SearchBytePattern(pattern, toBeSearched);

            int testpatchaddress = 0;
            foreach (var item in positions)
            {
                testpatchaddress = item;
                //MessageBox.Show("Pattern matched at pos " + item);
            }
            testpatchaddress = testpatchaddress + 8;
            string hextestpatchaddress = testpatchaddress.ToString("X");
            Properties.Settings.Default.testpatchaddress = testpatchaddress;
            Properties.Settings.Default.hextestpatchaddress = hextestpatchaddress;
            Properties.Settings.Default.Save();
            //MessageBox.Show("Hex: 0x" + hextestpatchaddress + "   " + testpatchaddress);
            return testpatchaddress;
        }
        public static void ApplyDefPatch()
        {
            try
            {
                using (var stream = new FileStream(BLPath, FileMode.Open, FileAccess.ReadWrite))
                {
                    stream.Position = Properties.Settings.Default.address;
                    stream.WriteByte(0x8B);
                    stream.Position = Properties.Settings.Default.address + 1;
                    stream.WriteByte(0x81);
                    stream.Position = Properties.Settings.Default.address + 2;
                    stream.WriteByte(0xB8);
                    stream.Position = Properties.Settings.Default.address + 3;
                    stream.WriteByte(0x01);
                    stream.Position = Properties.Settings.Default.address + 4;
                    stream.WriteByte(0x00);
                    stream.Position = Properties.Settings.Default.address + 5;
                    stream.WriteByte(0x00);
                    stream.Position = Properties.Settings.Default.address + 6;
                    stream.WriteByte(0x83);
                    stream.Position = Properties.Settings.Default.address + 7;
                    stream.WriteByte(0xF8);
                    stream.Position = Properties.Settings.Default.address + 8;
                    stream.WriteByte(0x27);
                    stream.Position = Properties.Settings.Default.address + 9;
                    stream.WriteByte(0x74);
                    stream.Position = Properties.Settings.Default.address + 10;
                    stream.WriteByte(0x01);
                    stream.Position = Properties.Settings.Default.address + 11;
                    stream.WriteByte(0xC3);
                    stream.Position = Properties.Settings.Default.address + 12;
                    stream.WriteByte(0x83);
                    stream.Position = Properties.Settings.Default.address + 13;
                    stream.WriteByte(0xC0);
                    stream.Position = Properties.Settings.Default.address + 14;
                    stream.WriteByte(0x05);
                    stream.Position = Properties.Settings.Default.address + 15;
                    stream.WriteByte(0xC3);
                    stream.Close();
                    Properties.Settings.Default.patched = "true";
                    Properties.Settings.Default.Save();
                }
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("Could not find Borderlands2.exe!");
            }
            catch (IOException excep)
            {
                MessageBox.Show("Could not open Borderlands2.exe. Is it opened by another application?");
            }
            catch (Exception excep)
            {
                MessageBox.Show("Something went wrong!\n{0}", excep.Message);
            }
            finally
            {
                GC.Collect();
            }
        }
        public static void RevertPatch()
        {
            try
            {
                using (var stream = new FileStream(BLPath, FileMode.Open, FileAccess.ReadWrite))
                {
                    stream.Position = Properties.Settings.Default.address;
                    stream.Write(new byte[] { 0x8B, 0x81, 0xB8, 0x01, 0x00, 0x00, 0xC3, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC }, 0, 16);
                    //stream.WriteByte(0x8B);
                    //stream.Position = Properties.Settings.Default.address + 1;
                    //stream.WriteByte(0x81);
                    //stream.Position = Properties.Settings.Default.address + 2;
                    //stream.WriteByte(0xB8);
                    //stream.Position = Properties.Settings.Default.address + 3;
                    //stream.WriteByte(0x01);
                    //stream.Position = Properties.Settings.Default.address + 4;
                    //stream.WriteByte(0x00);
                    //stream.Position = Properties.Settings.Default.address + 5;
                    //stream.WriteByte(0x00);
                    //stream.Position = Properties.Settings.Default.address + 6;
                    //stream.WriteByte(0xC3);
                    //stream.Position = Properties.Settings.Default.address + 7;
                    //stream.WriteByte(0xCC);
                    //stream.Position = Properties.Settings.Default.address + 8;
                    //stream.WriteByte(0xCC);
                    //stream.Position = Properties.Settings.Default.address + 9;
                    //stream.WriteByte(0xCC);
                    //stream.Position = Properties.Settings.Default.address + 10;
                    //stream.WriteByte(0xCC);
                    //stream.Position = Properties.Settings.Default.address + 11;
                    //stream.WriteByte(0xCC);
                    //stream.Position = Properties.Settings.Default.address + 12;
                    //stream.WriteByte(0xCC);
                    //stream.Position = Properties.Settings.Default.address + 13;
                    //stream.WriteByte(0xCC);
                    //stream.Position = Properties.Settings.Default.address + 14;
                    //stream.WriteByte(0xCC);
                    //stream.Position = Properties.Settings.Default.address + 15;
                    //stream.WriteByte(0xCC);


                    //Apply default values for Testpatch-Edit
                    //Triggervalue
                    stream.Position = Properties.Settings.Default.testpatchaddress;
                    stream.WriteByte(0x27);

                    //Slotsvalue
                    stream.Position = Properties.Settings.Default.testpatchaddress + 4;
                    //stream.WriteByte(0x27);
                    stream.Write(BitConverter.GetBytes(0x27), 0, 4);

                    stream.Close();
                    Properties.Settings.Default.patched = "false";
                    Properties.Settings.Default.Save();
                }
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("Could not find Borderlands2.exe!");
            }
            catch (IOException excep)
            {
                MessageBox.Show("Could not open Borderlands2.exe. Is it opened by another application?");
            }
            catch (Exception excep)
            {
                MessageBox.Show("Something went wrong!\n{0}", excep.Message);
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}
