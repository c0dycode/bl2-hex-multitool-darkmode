﻿/*
					Copyright 2017 c0dycode

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


using System.IO;
using System.IO.IsolatedStorage;

namespace BL2HexMultitool
{ 

    class FileStreamHelper
    {
        public static byte[] BLFile = File.ReadAllBytes(CheckPatched.BLPath);
        public static FileStream BLFileStream => new FileStream(CheckPatched.BLPath, FileMode.Open, FileAccess.ReadWrite);
    }
}
