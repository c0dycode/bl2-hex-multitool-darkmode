﻿/*
					Copyright 2017 c0dycode

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL2HexMultitool
{
    class SearchPattern
    {
        #region Pattern fields
        public static readonly byte[] LevelCheck1Pattern = { 0xC3, 0x55, 0x8B, 0xEC, 0x83, 0x3D, 0xB8 }; // Final address is at "-1" from result
        public static readonly byte[] LevelCheck2Pattern = { 0x55, 0x8B, 0xEC, 0x83, 0xEC, 0x08, 0x56, 0x57, 0x89, 0x4D, 0xF8 }; // Final address is at "+11" from result
        public static readonly byte[] LevelCheckScalingPattern = { 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0xBF }; // Final address is at "-5" from result
        public static readonly byte[] LevelCheckEnemyScalingPattern = { 0x01, 0x00, 0x00, 0x00, 0x5E, 0x8B, 0xE5, 0x5D, 0xC3, 0x3B, 0xF0 }; // Final address is at "+11" from result

        // Old Eridium Pattern
        // public static readonly byte[] EridiumPattern = { 0x60, 0xB1, 0xD7, 0x00, 0x20, 0x16, 0x00, 0x01, 0x60, 0x3A, 0x45, 0x00, 0xFF, 0xE0, 0xF5, 0x05 }; // Final address is the same as result
        public static readonly byte[] EridiumPattern = { 0xE7, 0x03, 0x00, 0x00, 0xE7, 0x03, 0x00, 0x00, 0xE7, 0x03, 0x00,
            0x00, 0xE7, 0x03, 0x00, 0x00, 0xE7, 0x03, 0x00, 0x00, 0xE7, 0x03, 0x00, 0x00, 0xE7, 0x03, 0x00, 0x00, 0xE7, 0x03, 0x00, 0x00, 0x50, 0x78 }; 
        // Final addresses are as followed:
        // -20 = Max Cash, -16 = Max Eridium, -12 = Max Seraph, -8 = Unknown, -4 = Torque

        public static readonly byte[] BackpackPattern = { 0x0C, 0x00, 0x00, 0x00, 0xEB, 0x0A, 0x83, 0xF8 }; // Final address is "+8" from result
        public static readonly byte[] BackpackDefaultPattern = { 204, 204, 139, 129, 184, 1, 0, 0, 195 }; // Final address is "+2" from result

        public static readonly byte[] ArrayLimitPattern = { 0x05, 0xB9, 0x64, 0x00, 0x00, 0x00, 0x3B, 0xF9, 0x0F, 0x8D }; // Final address is "-1" from result
        public static readonly byte[] ArrayLimitConsoleMessagePattern = { 0x7B, 0x00, 0x00, 0x00, 0x8B, 0x8D, 0x9C, 0xEE, 0xFF, 0xFF, 0x83, 0xC0, 0x9D, 0x50 }; // Final address is "-1" from result
        #endregion

        /// <summary>
        /// Search a given Bytearray in another Bytearray
        /// </summary>
        /// <param name="pattern">The Bytearray to search for</param>
        /// <param name="bytes">The Bytearray where to look for the given Bytearray</param>
        /// <returns></returns>
        public static List<int> SearchBytePattern(byte[] pattern, byte[] bytes)
        {
            List<int> positions = new List<int>();
            int patternLength = pattern.Length;
            int totalLength = bytes.Length;
            byte firstMatchByte = pattern[0];
            for (int i = 0; i < totalLength; i++)
            {
                if (firstMatchByte == bytes[i] && totalLength - i >= patternLength)
                {
                    byte[] match = new byte[patternLength];
                    Array.Copy(bytes, i, match, 0, patternLength);
                    if (match.SequenceEqual<byte>(pattern))
                    {
                        positions.Add(i);

                        // break, since we can only have a single result
                        break;
                        //i += patternLength - 1;
                    }
                }
            }
            
            return positions;
        }
    }
}
