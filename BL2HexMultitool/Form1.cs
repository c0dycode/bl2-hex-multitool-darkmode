﻿/*
					Copyright 2017 c0dycode

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Timers;
using System.Windows.Forms;
using Timer = System.Timers.Timer;

namespace BL2HexMultitool
{
    public partial class BL2HMTT : Form
    {
        Eridium Erid = new Eridium();
        MaxLVL mLVL = new MaxLVL();
        BackPack BP = new BackPack();
        

        public BL2HMTT()
        {
            InitializeComponent();
        }

        private void btnEridium_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                Erid.ShowDialog();
                this.Show();
            }
            catch (ObjectDisposedException excep)
            {
                MessageBox.Show("Something went wrong! {0}", excep.Message);
            }
        }

        private void btnMaxLevel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                mLVL.ShowDialog();
                this.Show();
            }
            catch (ObjectDisposedException excep)
            {
                MessageBox.Show("Something went wrong! {0}", excep.Message);
            }
        }

        private void btnBackPack_Click(object sender, EventArgs e)
        {
            this.Hide();
            BP.ShowDialog();
            this.Show();
        }

        private void btnArrayLimit_Click(object sender, EventArgs e)
        {

        }

        private void BL2HMTT_Load(object sender, EventArgs e)
        {
            LblApplied.Hide();
            // Get current status of the WeaponBypass
            CbWeaponSanity.Checked = Weapons.WeaponSanityStatus();
            // Get current status of the ItemBypass (currently disabled in Release-build
            CbItemsSanity.Checked = Items.ItemSanityStatus();
            // Reading and displaying current Version
            LblVersion.Text = "Version: " + 
                Assembly.GetAssembly(GetType()).GetName().Version.Major.ToString() 
                + "." +
                Assembly.GetAssembly(GetType()).GetName().Version.Minor;
            CbArrayLimit.SelectedIndexChanged += null;
            ArrayLimit.ArrayLimitAddress =
                CheckPatched.ScanForPatternAddress(SearchPattern.ArrayLimitPattern, -1);
            ArrayLimit.ArrayLimitMessageAddress =
                CheckPatched.ScanForPatternAddress(SearchPattern.ArrayLimitConsoleMessagePattern, -1);
            var arrayStatus = ArrayLimit.ReadArrayLimitStatus();
            CbArrayLimit.SelectedItem = (arrayStatus == 117) ? ArrayLimit.ArrayStatusEnum.Disabled : ArrayLimit.ArrayStatusEnum.Enabled;

            CbArrayLimit.SelectedIndexChanged += CbArrayLimit_SelectedIndexChanged;
        }
        

        private void CbWeaponSanity_CheckedChanged(object sender, EventArgs e)
        {
            if (CbWeaponSanity.Checked == false)
            {
                Weapons.DisableWeaponBypass();
            }
            else if (CbWeaponSanity.Checked == true)
            {
                Weapons.EnableWeaponBypass();
            }
            else
            {
                MessageBox.Show("Something went wrong!");
            }
            CbWeaponSanity.Checked = Weapons.CurrentWeapBypass;
        }

        private void CbItemsSanity_CheckedChanged(object sender, EventArgs e)
        {

            if (CbItemsSanity.Checked == false)
            {
                Items.DisableItemBypass();
            }
            else if (CbItemsSanity.Checked == true)
            {
                Items.EnableItemBypass();
            }
            else
            {
                MessageBox.Show("Something went wrong!");
            }
            CbItemsSanity.Checked = Items.CurrentItemBypass;
        }

        private void CbArrayLimit_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
               
                if (CbArrayLimit.SelectedItem.Equals(ArrayLimit.ArrayStatusEnum.Enabled))
                {
                    ArrayLimit.RestoreArrayLimit();
                    System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
                    timer.Interval = 2000;
                    timer.Tick += (o, args) =>
                    {
                        LblApplied.Hide();
                        timer.Stop();
                    };
                    timer.Start();
                    LblApplied.Show();
                }
                else if (CbArrayLimit.SelectedItem.Equals(ArrayLimit.ArrayStatusEnum.Disabled))
                {
                    ArrayLimit.SetArrayLimit();
                    System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
                    timer.Interval = 2000;
                    timer.Tick += (o, args) =>
                    {
                        LblApplied.Hide();
                        timer.Stop();
                    };
                    timer.Start();
                    LblApplied.Show();
                }

            }
            catch (NullReferenceException nrEx)
            {
                // TODO: Handle this properly
                // Ignore this for now
            }
            
        }
    }
}
