﻿/*
					Copyright 2017 c0dycode

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


using System;
using System.ComponentModel;
using System.Diagnostics.Eventing.Reader;
using System.IO;
using System.Windows.Forms;

namespace BL2HexMultitool
{
    public class ArrayLimit
    {
        public static int ArrayLimitAddress { get; set; }
        public static int ArrayLimitMessageAddress { get; set; }

        /// <summary>
        /// Removes the ArrayLimit from the game
        /// </summary>
        public static void SetArrayLimit()
        {
            using (var stream = new FileStream(CheckPatched.BLPath, FileMode.Open, FileAccess.ReadWrite))
            {
                stream.Position = ArrayLimitAddress;
                if (stream.Position > 1)
                    stream.WriteByte(0x75);

                stream.Position = ArrayLimitMessageAddress;
                if (stream.Position > 1)
                    stream.WriteByte(0x85);
            }
        }

        /// <summary>
        /// Restores the ArrayLimit in the game
        /// </summary>
        public static void RestoreArrayLimit()
        {
            using (var stream = new FileStream(CheckPatched.BLPath, FileMode.Open, FileAccess.ReadWrite))
            {
                stream.Position = ArrayLimitAddress;
                stream.WriteByte(0x7E);

                stream.Position = ArrayLimitMessageAddress;
                stream.WriteByte(0x8C);
            }
        }

        /// <summary>
        /// Read the current status of the ArrayLimit
        /// </summary>
        /// <returns></returns>
        public static int ReadArrayLimitStatus()
        {
            using (var stream = new FileStream(CheckPatched.BLPath, FileMode.Open, FileAccess.ReadWrite))
            {
                stream.Position = ArrayLimitAddress;
                return stream.ReadByte();
            }
        }

        public enum ArrayStatusEnum
        {   
            Enabled,
            Disabled
        }
    }
}
