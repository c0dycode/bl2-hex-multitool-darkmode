﻿/*
					Copyright 2017 c0dycode

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


using System;
using System.Collections.Generic;
using System.IO;

namespace BL2HexMultitool
{
    public class Currencies
    {
        public static int MaxEridium { get; set; } = 500;
        public static int MaxSeraph { get; set; } = 999;
        public static int MaxTorque { get; set; } = 999;
        public static int MaxCash { get; set; } = 99999999;

        public enum Targets
        {
            Cash,
            Eridium,
            Seraph,
            Torque
        }

        /// <summary>
        /// Gets the maximum currency at the given address
        /// </summary>
        /// <param name="address">The address where to read from</param>
        /// <returns></returns>
        public int GetCurrency(int address)
        {
            using (var stream = new FileStream(CheckPatched.BLPath, FileMode.Open, FileAccess.ReadWrite))
            {
                List<byte> eridlist = new List<byte>();
                stream.Position = address;
                for (int i = 0; i < 4; i++)
                {
                    int eridvalue = stream.ReadByte();
                    byte beridvalue = Convert.ToByte(eridvalue);
                    eridlist.Add(beridvalue);
                }
                byte[] temparray = eridlist.ToArray();
                int finaleridvalue = BitConverter.ToInt32(temparray, 0);
                MaxEridium = finaleridvalue;

                return MaxEridium;
            }
        }

        /// <summary>
        /// Set the given currency at the given address
        /// </summary>
        /// <param name="address">Address of where to write to</param>
        /// <param name="target">enum of Targets to determine the value needed to write</param>
        public void SetCurrency(int address, Targets target)
        {
            using (var stream = new FileStream(CheckPatched.BLPath, FileMode.Open, FileAccess.ReadWrite))
            {
                byte[] finalbytes = new byte[] { };
                switch (target)
                {
                    case Targets.Cash:
                        finalbytes = BitConverter.GetBytes(MaxCash);
                        break;
                    case Targets.Eridium:
                        finalbytes = BitConverter.GetBytes(MaxEridium);
                        break;
                    case Targets.Seraph:
                        finalbytes = BitConverter.GetBytes(MaxSeraph);
                        break;
                    case Targets.Torque:
                        finalbytes = BitConverter.GetBytes(MaxTorque);
                        break;
                }
                //byte[] finalbytes = BitConverter.GetBytes(MaxEridium);

                stream.Position = address;
                stream.Write(finalbytes, 0, 4);
            }
        }
    }
}
