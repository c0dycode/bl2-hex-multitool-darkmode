﻿/*
					Copyright 2017 c0dycode

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


using System;
using System.Collections.Generic;
using System.IO;

namespace BL2HexMultitool
{ 
    public class PlayerLevel
    {
        public static int MaxLevel { get; set; } = 72;
        public static int ScalingAddress { get; set; }

        public int DefaultMaxLevel { get; set; } = 72;
        private readonly List<byte[]> _patternByteList = new List<byte[]>();

        /// <summary>
        /// Adds all the needed Patterns to a List of Bytearrays
        /// </summary>
        public PlayerLevel()
        {
            // All patterns can be found in ../Helpers/SearchPattern.cs
            _patternByteList.Add(SearchPattern.LevelCheck1Pattern);
            _patternByteList.Add(SearchPattern.LevelCheck2Pattern);
            _patternByteList.Add(SearchPattern.LevelCheckScalingPattern);
            _patternByteList.Add(SearchPattern.LevelCheckEnemyScalingPattern);
        }

        /// <summary>
        /// Find all addresses for patterns added by the constructor
        /// </summary>
        /// <returns></returns>
        public List<int> GetLevelAddresses()
        {
            List<int> tempList = new List<int>(4);
            foreach (var pattern in _patternByteList)
            {
                tempList.Add(CheckPatched.ScanForPatternAddress(pattern));
            }
            return tempList;
        }

        /// <summary>
        /// Applying Patches related to increasing MaxLevel
        /// </summary>
        /// <param name="addressv1">Address for BaseMaxLevel (without DLC) - 50</param>
        /// <param name="addressv2">Address of a functioncall, which is related to stop gaining XP at Level 72</param>
        /// <param name="addressEnemyScaling">Address of conditional jump ("JNL"/"JGE"). If it's not "NOP"ed, Enemies don't keep scaling</param>
        public void ApplyLevelPatch(int addressv1, int addressv2, int addressEnemyScaling)
        {
            using (var stream = new FileStream(CheckPatched.BLPath, FileMode.Open, FileAccess.ReadWrite))
                {
                // Hardcoding the default max lvl without DLC
                // IIRC it usually is loaded dynamically and would break OP levels if not hardcoded
                stream.Position = addressv1;
                stream.WriteByte(0x32);

                // Overwriting a functioncall
                // This basically increases the base-max-level without DLC (50) to allow you to keep gaining XP
                // Calculation = Chosen max Level - 22
                byte finalvalue = Convert.ToByte(MaxLevel - 22);

                stream.Position = addressv2;
                stream.WriteByte(0xB8);
                stream.Position = addressv2 + 1;
                stream.WriteByte(finalvalue);
                stream.Position = addressv2 + 2;
                stream.WriteByte(0x00);
                stream.Position = addressv2 + 3;
                stream.WriteByte(0x00);
                stream.Position = addressv2 + 4;
                stream.WriteByte(0x00);

                // Calculating the DLC Levelamount for the UVHM DLC
                // Calculation = Chosen MaxLevel - 61 (That's 50 baselevel + first
                int finalscalingvalue = MaxLevel - 72;
                byte basescaling = 11;
                byte calculatedscaling = Convert.ToByte(MaxLevel - 61);
                if (finalscalingvalue > 0)
                {
                    basescaling = Convert.ToByte(finalscalingvalue + 11);
                }
                stream.Position = ScalingAddress;
                stream.WriteByte(calculatedscaling);

                // Disables/Removes a jump that allows enemies to keep scaling with you
                stream.Position = addressEnemyScaling;
                stream.WriteByte(0x90);
                stream.Position = addressEnemyScaling + 1;
                stream.WriteByte(0x90);
            }
        }

        /// <summary>
        /// Removing Patches related to increasing MaxLevel
        /// </summary>
        /// <param name="addressv1">Address for BaseMaxLevel (without DLC) - 50</param>
        /// <param name="addressv2">Address of a functioncall, which is related to stop/keep gaining XP at Level 72</param>
        /// <param name="addressEnemyScaling">Address of conditional jump ("JNL"/"JGE"). If it's not "NOP"ed, Enemies don't keep scaling</param>
        public void RemoveLevelPatch(int addressv1, int addressv2, int addressEnemyScaling)
        {
            using (var stream = new FileStream(CheckPatched.BLPath, FileMode.Open, FileAccess.ReadWrite))
            {
                byte finalvalue = Convert.ToByte(MaxLevel - 22);

                // Restoring default Max Level without DLC (50)
                stream.Position = addressv1;
                stream.WriteByte(0x32);

                // Restoring a functioncall
                // This function should return max possible level to gain xp on, IIRC
                stream.Position = addressv2;
                stream.WriteByte(0xE8);
                stream.Position = addressv2 + 1;
                stream.WriteByte(0x60);
                stream.Position = addressv2 + 2;
                stream.WriteByte(0x26);
                stream.Position = addressv2 + 3;
                stream.WriteByte(0x1A);
                stream.Position = addressv2 + 4;
                stream.WriteByte(0xFF);


                //Restore Enemy-Scaling behaviour, so they stop at X
                stream.Position = ScalingAddress;
                stream.WriteByte(0x0B);
                stream.Position = addressEnemyScaling;
                stream.WriteByte(0x7D);
                stream.Position = addressEnemyScaling + 1;
                stream.WriteByte(0x02);
            }
        }
    }
}
