﻿using System;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;

namespace BL2HexMultitool
{
    partial class BL2HMTT
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BL2HMTT));
            this.lblHeader = new System.Windows.Forms.Label();
            this.BtnCurrencies = new System.Windows.Forms.Button();
            this.btnMaxLevel = new System.Windows.Forms.Button();
            this.btnBackPack = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.CbWeaponSanity = new System.Windows.Forms.CheckBox();
            this.CbItemsSanity = new System.Windows.Forms.CheckBox();
            this.LblDisabledForNow = new System.Windows.Forms.Label();
            this.LblVersion = new System.Windows.Forms.Label();
            this.CbArrayLimit = new System.Windows.Forms.ComboBox();
            this.arrayStatusEnumBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.LblArrayLimit = new System.Windows.Forms.Label();
            this.LblApplied = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.arrayStatusEnumBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // lblHeader
            // 
            this.lblHeader.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblHeader.AutoSize = true;
            this.lblHeader.Location = new System.Drawing.Point(122, 10);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(86, 13);
            this.lblHeader.TabIndex = 0;
            this.lblHeader.Text = "Choose a Patch!";
            this.lblHeader.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // BtnCurrencies
            // 
            this.BtnCurrencies.BackColor = Color.FromArgb(40, 40, 40);
            this.BtnCurrencies.ForeColor = Color.White;
            this.BtnCurrencies.Location = new System.Drawing.Point(12, 35);
            this.BtnCurrencies.Name = "BtnCurrencies";
            this.BtnCurrencies.Size = new System.Drawing.Size(75, 23);
            this.BtnCurrencies.TabIndex = 1;
            this.BtnCurrencies.Text = "Currencies";
            this.BtnCurrencies.UseVisualStyleBackColor = false;
            this.BtnCurrencies.Click += new System.EventHandler(this.btnEridium_Click);
            // 
            // btnMaxLevel
            // 
            this.btnMaxLevel.BackColor = Color.FromArgb(40, 40, 40);
            this.btnMaxLevel.ForeColor = Color.White;
            this.btnMaxLevel.Location = new System.Drawing.Point(93, 35);
            this.btnMaxLevel.Name = "btnMaxLevel";
            this.btnMaxLevel.Size = new System.Drawing.Size(75, 23);
            this.btnMaxLevel.TabIndex = 2;
            this.btnMaxLevel.Text = "Max Level";
            this.btnMaxLevel.UseVisualStyleBackColor = false;
            this.btnMaxLevel.Click += new System.EventHandler(this.btnMaxLevel_Click);
            // 
            // btnBackPack
            // 
            this.btnBackPack.BackColor = Color.FromArgb(40, 40, 40);
            this.btnBackPack.ForeColor = Color.White;
            this.btnBackPack.Location = new System.Drawing.Point(174, 35);
            this.btnBackPack.Name = "btnBackPack";
            this.btnBackPack.Size = new System.Drawing.Size(75, 23);
            this.btnBackPack.TabIndex = 3;
            this.btnBackPack.Text = "Backpack";
            this.btnBackPack.UseVisualStyleBackColor = false;
            this.btnBackPack.Click += new System.EventHandler(this.btnBackPack_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Disable Sanity Checks";
            // 
            // CbWeaponSanity
            // 
            this.CbWeaponSanity.AutoSize = true;
            this.CbWeaponSanity.Location = new System.Drawing.Point(16, 82);
            this.CbWeaponSanity.Name = "CbWeaponSanity";
            this.CbWeaponSanity.Size = new System.Drawing.Size(72, 17);
            this.CbWeaponSanity.TabIndex = 6;
            this.CbWeaponSanity.Text = "Weapons";
            this.CbWeaponSanity.UseVisualStyleBackColor = true;
            this.CbWeaponSanity.CheckedChanged += new System.EventHandler(this.CbWeaponSanity_CheckedChanged);
            // 
            // CbItemsSanity
            // 
            this.CbItemsSanity.AutoSize = true;
            this.CbItemsSanity.Enabled = false;
            this.CbItemsSanity.Location = new System.Drawing.Point(16, 102);
            this.CbItemsSanity.Name = "CbItemsSanity";
            this.CbItemsSanity.Size = new System.Drawing.Size(51, 17);
            this.CbItemsSanity.TabIndex = 7;
            this.CbItemsSanity.Text = "Items";
            this.CbItemsSanity.UseVisualStyleBackColor = false;
            this.CbItemsSanity.CheckedChanged += new System.EventHandler(this.CbItemsSanity_CheckedChanged);
            this.CbItemsSanity.Hide();
            // 
            // LblDisabledForNow
            // 
            this.LblDisabledForNow.AutoSize = true;
            this.LblDisabledForNow.Enabled = false;
            this.LblDisabledForNow.ForeColor = System.Drawing.Color.CadetBlue;
            this.LblDisabledForNow.Location = new System.Drawing.Point(73, 103);
            this.LblDisabledForNow.Name = "LblDisabledForNow";
            this.LblDisabledForNow.Size = new System.Drawing.Size(130, 13);
            this.LblDisabledForNow.TabIndex = 8;
            this.LblDisabledForNow.Text = "Disabled! At least for now!";
            this.LblDisabledForNow.Hide();
            // 
            // LblVersion
            // 
            this.LblVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.LblVersion.AutoSize = true;
            this.LblVersion.Location = new System.Drawing.Point(269, 103);
            this.LblVersion.Name = "LblVersion";
            this.LblVersion.Size = new System.Drawing.Size(0, 13);
            this.LblVersion.TabIndex = 9;
            // 
            // CbArrayLimit
            // 
            this.CbArrayLimit.BackColor = Color.FromArgb(40, 40, 40);
            this.CbArrayLimit.ForeColor = Color.White;
            this.CbArrayLimit.DataSource = this.arrayStatusEnumBindingSource;
            this.CbArrayLimit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbArrayLimit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.CbArrayLimit.FormattingEnabled = true;
            
            this.CbArrayLimit.Location = new System.Drawing.Point(255, 36);
            this.CbArrayLimit.Name = "CbArrayLimit";
            this.CbArrayLimit.Size = new System.Drawing.Size(75, 15);
            this.CbArrayLimit.TabIndex = 10;
            // 
            // arrayStatusEnumBindingSource
            // 
            this.arrayStatusEnumBindingSource.DataSource = new BL2HexMultitool.ArrayLimit.ArrayStatusEnum[] {
        BL2HexMultitool.ArrayLimit.ArrayStatusEnum.Enabled,
        BL2HexMultitool.ArrayLimit.ArrayStatusEnum.Disabled};
            this.arrayStatusEnumBindingSource.Position = 0;
            // 
            // LblArrayLimit
            // 
            this.LblArrayLimit.AutoSize = true;
            this.LblArrayLimit.Location = new System.Drawing.Point(256, 22);
            this.LblArrayLimit.Name = "LblArrayLimit";
            this.LblArrayLimit.Size = new System.Drawing.Size(55, 13);
            this.LblArrayLimit.TabIndex = 11;
            this.LblArrayLimit.Text = "Array-Limit";
            // 
            // LblApplied
            // 
            this.LblApplied.AutoSize = true;
            this.LblApplied.Location = new System.Drawing.Point(259, 64);
            this.LblApplied.Name = "LblApplied";
            this.LblApplied.Size = new System.Drawing.Size(45, 13);
            this.LblApplied.TabIndex = 12;
            this.LblApplied.Text = "Applied!";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(-1, -1);
            this.label2.Name = "byC0dycode";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "by c0dycode";
            // 
            // BL2HMTT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.ClientSize = new System.Drawing.Size(337, 120);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.LblApplied);
            this.Controls.Add(this.LblArrayLimit);
            this.Controls.Add(this.CbArrayLimit);
            this.Controls.Add(this.LblVersion);
            this.Controls.Add(this.LblDisabledForNow);
            this.Controls.Add(this.CbItemsSanity);
            this.Controls.Add(this.CbWeaponSanity);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnBackPack);
            this.Controls.Add(this.btnMaxLevel);
            this.Controls.Add(this.BtnCurrencies);
            this.Controls.Add(this.lblHeader);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BL2HMTT";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "BL2 Hex-Multitool";
            this.Load += new System.EventHandler(this.BL2HMTT_Load);
            ((System.ComponentModel.ISupportInitialize)(this.arrayStatusEnumBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

#endregion

        private System.Windows.Forms.Label lblHeader;
        private System.Windows.Forms.Button BtnCurrencies;
        private System.Windows.Forms.Button btnMaxLevel;
        private System.Windows.Forms.Button btnBackPack;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox CbWeaponSanity;
        private System.Windows.Forms.CheckBox CbItemsSanity;
        private System.Windows.Forms.Label LblDisabledForNow;
        private System.Windows.Forms.Label LblVersion;
        private System.Windows.Forms.ComboBox CbArrayLimit;
        private System.Windows.Forms.BindingSource arrayStatusEnumBindingSource;
        private System.Windows.Forms.Label LblArrayLimit;
        private System.Windows.Forms.Label LblApplied;
        private Label label2;
    }
}

