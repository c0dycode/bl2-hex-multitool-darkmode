﻿/*
					Copyright 2017 c0dycode

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace BL2HexMultitool
{
    class Weapons
    {
        #region Enabled Pattern
        #region Patterns to use in code
        /*
        BodyAOB:
        0x83, 0x7F, 0x10, 0xFF, 0x8D, 0x47, 0x10, 0x75

        GripAOB:
        0x83, 0x7F, 0x14, 0xFF, 0x8D, 0x47, 0x14, 0x75

        BarAOB:
        0x83, 0x7F, 0x18, 0xFF, 0x8D, 0x47, 0x18, 0x75

        SightAOB:
        0x83, 0x7F, 0x1C, 0xFF, 0x8D, 0x47, 0x1C, 0x75

        StockAOB:
        0x83, 0x7F, 0x20, 0xFF, 0x8D, 0x47, 0x20, 0x75

        EleAOB:
        0x83, 0x7F, 0x24, 0xFF, 0x8D, 0x47, 0x24, 0x75

        Acc1AOB:
        0x83, 0x7F, 0x28, 0xFF, 0x8D, 0x47, 0x28, 0x75

        Acc2AOB:
        0x83, 0x7F, 0x2C, 0xFF, 0x8D, 0x47, 0x2C, 0x75

        MatAOB:
        0x83, 0x7F, 0x30, 0xFF, 0x8D, 0x47, 0x30, 0x75
        */
        #endregion

        #region Blank Patterns
        /*
        BodyAOB:
        83 7F 10 FF 8D 47 10 75

        GripAOB:
        83 7F 14 FF 8D 47 14 75

        BarAOB:
        83 7F 18 FF 8D 47 18 75

        SightAOB:
        83 7F 1C FF 8D 47 1C 75

        StockAOB:
        83 7F 20 FF 8D 47 20 75

        EleAOB:
        83 7F 24 FF 8D 47 24 75

        Acc1AOB:
        83 7F 28 FF 8D 47 28 75

        Acc2AOB:
        83 7F 2C FF 8D 47 2C 75

        MatAOB:
        83 7F 30 FF 8D 47 30 75
        */
        #endregion
        #endregion

        #region Disabled Patterns
        #region Patterns to use in code
        /*   
        BodCheck:
        0x83, 0x7F, 0x10, 0x00, 0x8D, 0x47, 0x10, 0x74

        GripCheck:
        0x83, 0x7F, 0x14, 0x00, 0x8D, 0x47, 0x14, 0x74

        BarCheck:
        0x83, 0x7F, 0x18, 0x00, 0x8D, 0x47, 0x18, 0x74

        SightCheck:
        0x83, 0x7F, 0x1C, 0x00, 0x8D, 0x47, 0x1C, 0x74

        StockCheck:
        0x83, 0x7F, 0x20, 0x00, 0x8D, 0x47, 0x20, 0x74

        EleCheck:
        0x83, 0x7F, 0x24, 0x00, 0x8D, 0x47, 0x24, 0x74

        Acc1Check:
        0x83, 0x7F, 0x28, 0x00, 0x8D, 0x47, 0x28, 0x74

        Acc2Check:
        0x83, 0x7F, 0x2C, 0x00, 0x8D, 0x47, 0x2C, 0x74

        MatCheck:
        0x83, 0x7F, 0x30, 0x00, 0x8D, 0x47, 0x30, 0x74
        */
        #endregion

        #region Blank Patterns
        /*   
        BodCheck:
        83 7F 10 00 8D 47 10 74

        GripCheck:
        83 7F 14 00 8D 47 14 74

        BarCheck:
        83 7F 18 00 8D 47 18 74

        SightCheck:
        83 7F 1C 00 8D 47 1C 74

        StockCheck:
        83 7F 20 00 8D 47 20 74

        EleCheck:
        83 7F 24 00 8D 47 24 74

        Acc1Check:
        83 7F 28 00 8D 47 28 74

        Acc2Check:
        83 7F 2C 00 8D 47 2C 74

        MatCheck:
        83 7F 30 00 8D 47 30 74
        */
        #endregion
        #endregion

        public static List<int> WeapSanityAddressList = new List<int>();
        public static bool CurrentWeapBypass { get; set; }

        private static void GetDisabledPositions()
        {
            WeaponPattern.BytePatternListDisabled.Clear();
            WeaponPattern.BytePatternListEnabled.Clear();
            var WP = new WeaponPattern();

            foreach (var aob in WeaponPattern.BytePatternListDisabled)
            {
                WeapSanityAddressList.AddRange(SearchPattern.SearchBytePattern(aob, File.ReadAllBytes(CheckPatched.BLPath)));
                if (WeapSanityAddressList.Count > 7)
                    CurrentWeapBypass = false;
            }
        }

        private static void GetEnabledPositions()
        {
            WeaponPattern.BytePatternListDisabled.Clear();
            WeaponPattern.BytePatternListEnabled.Clear();
            WeapSanityAddressList.Clear();
            var WP = new WeaponPattern();

            // For each Pattern, get the address
            foreach (var aob in WeaponPattern.BytePatternListEnabled)
            {
                WeapSanityAddressList.AddRange(SearchPattern.SearchBytePattern(aob, File.ReadAllBytes(CheckPatched.BLPath)));
                
                // If all patterns were found, we have the address of all patterns and therefore the Weapon-Bypass is already enabled
                if (WeapSanityAddressList.Count > 7)
                   CurrentWeapBypass = true;
            }
        }

        public static void EnableWeaponBypass()
        {
            WeapSanityAddressList.Clear();
            GetDisabledPositions();

            try
            {
                using (var stream = new FileStream(CheckPatched.BLPath, FileMode.Open, FileAccess.ReadWrite))
                {
                    foreach (var address in WeapSanityAddressList)
                    {
                        stream.Position = address + 3;
                        stream.WriteByte(0xFF);
                        stream.Position = address + 7;
                        stream.WriteByte(0x75);
                    }
                    stream.Close();
                }
                CurrentWeapBypass = true;

            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("Could not find Borderlands2.exe!");
            }
            catch (IOException excep)
            {
                MessageBox.Show("Could not open Borderlands2.exe. Is it opened by another application?");
            }
            catch (Exception excep)
            {
                MessageBox.Show("Something went wrong!\n{0}", excep.Message);
            }
            finally
            {
                GC.Collect();
            }
        }

        public static void DisableWeaponBypass()
        {
            WeapSanityAddressList.Clear();
            GetEnabledPositions();

            try
            {
                using (var stream = new FileStream(CheckPatched.BLPath, FileMode.Open, FileAccess.ReadWrite))
                {
                    foreach (var address in WeapSanityAddressList)
                    {
                        stream.Position = address + 3;
                        stream.WriteByte(0x00);
                        stream.Position = address + 7;
                        stream.WriteByte(0x74);
                    }
                    stream.Close();
                }
                CurrentWeapBypass = false;

            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("Could not find Borderlands2.exe!");
            }
            catch (IOException excep)
            {
                MessageBox.Show("Could not open Borderlands2.exe. Is it opened by another application?");
            }
            catch (Exception excep)
            {
                MessageBox.Show("Something went wrong!\n{0}", excep.Message);
            }
            finally
            {
                GC.Collect();
            }
        }

        public static bool WeaponSanityStatus()
        {
            GetEnabledPositions();
            return CurrentWeapBypass;
        }

    }

    public class WeaponPattern
    {
        public static List<byte[]> BytePatternListDisabled = new List<byte[]>();
        public static List<byte[]> BytePatternListEnabled = new List<byte[]>();

        public WeaponPattern()
        {
            #region Add all "Disabled" Patterns to DisabledPatternList
            // Body
            BytePatternListDisabled.Add(new byte[] { 0x83, 0x7F, 0x10, 0x00, 0x8D, 0x47, 0x10, 0x74 });
            // Grip
            BytePatternListDisabled.Add(new byte[] { 0x83, 0x7F, 0x14, 0x00, 0x8D, 0x47, 0x14, 0x74 });
            // Barrel
            BytePatternListDisabled.Add(new byte[] { 0x83, 0x7F, 0x18, 0x00, 0x8D, 0x47, 0x18, 0x74 });
            // Sight
            BytePatternListDisabled.Add(new byte[] { 0x83, 0x7F, 0x1C, 0x00, 0x8D, 0x47, 0x1C, 0x74 });
            // Stock
            BytePatternListDisabled.Add(new byte[] { 0x83, 0x7F, 0x20, 0x00, 0x8D, 0x47, 0x20, 0x74, 0x11 });
            // Element
            BytePatternListDisabled.Add(new byte[] { 0x83, 0x7F, 0x24, 0x00, 0x8D, 0x47, 0x24, 0x74 });
            // Accessory 1
            BytePatternListDisabled.Add(new byte[] { 0x83, 0x7F, 0x28, 0x00, 0x8D, 0x47, 0x28, 0x74 });
            // Accessory 2
            BytePatternListDisabled.Add(new byte[] { 0x83, 0x7F, 0x2C, 0x00, 0x8D, 0x47, 0x2C, 0x74 });
            //Material
            BytePatternListDisabled.Add(new byte[] { 0x83, 0x7F, 0x30, 0x00, 0x8D, 0x47, 0x30, 0x74, 0x11});
#endregion

#region Add all "Enabled" Patterns to EnabledPatternList
            // Body
            BytePatternListEnabled.Add(new byte[] { 0x83, 0x7F, 0x10, 0xFF, 0x8D, 0x47, 0x10, 0x75 });
            // Grip
            BytePatternListEnabled.Add(new byte[] { 0x83, 0x7F, 0x14, 0xFF, 0x8D, 0x47, 0x14, 0x75 });
            // Barrel
            BytePatternListEnabled.Add(new byte[] { 0x83, 0x7F, 0x18, 0xFF, 0x8D, 0x47, 0x18, 0x75 });
            // Sight
            BytePatternListEnabled.Add(new byte[] { 0x83, 0x7F, 0x1C, 0xFF, 0x8D, 0x47, 0x1C, 0x75 });
            // Stock
            BytePatternListEnabled.Add(new byte[] { 0x83, 0x7F, 0x20, 0xFF, 0x8D, 0x47, 0x20, 0x75 });
            // Element
            BytePatternListEnabled.Add(new byte[] { 0x83, 0x7F, 0x24, 0xFF, 0x8D, 0x47, 0x24, 0x75 });
            // Accessory 1
            BytePatternListEnabled.Add(new byte[] { 0x83, 0x7F, 0x28, 0xFF, 0x8D, 0x47, 0x28, 0x75 });
            // Accessory 2
            BytePatternListEnabled.Add(new byte[] { 0x83, 0x7F, 0x2C, 0xFF, 0x8D, 0x47, 0x2C, 0x75 });
            //Material
            BytePatternListEnabled.Add(new byte[] { 0x83, 0x7F, 0x30, 0xFF, 0x8D, 0x47, 0x30, 0x75 });
#endregion
        }

        /// <summary>
        /// Adding the patterns to the DisabledPatternList
        /// </summary>
        /// <param name="patternBytes">Array of bytes to add to the Disabled PatternList</param>
        public static void PatternBytesDisabled(byte[] patternBytes)
        {
            BytePatternListDisabled.Add(patternBytes);
        }

        /// <summary>
        /// Adding the patterns to the EnabledPatternList
        /// </summary>
        /// <param name="patternBytes">Array of bytes to add to the Enabled PatternList</param>
        public static void PatternBytesEnabled(byte[] patternBytes)
        {
            BytePatternListEnabled.Add(patternBytes);
        }
    }
}
