﻿/*
					Copyright 2017 c0dycode

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace BL2HexMultitool
{
    /// <summary>
    /// Patch to enable or disable the SanityCheck-Bypass for Items
    /// </summary>
    class Items
    {
        // TODO: If there is a way to make item-sanity-checks patchable
        // This needs to be updated


        //Patterns to look for when the Bypass is enabled
        #region Enabled Patterns
        #region Patterns to use in code
        /* 
        // AlphaAOB
        0x83, 0x7E, 0x10, 0xFF, 0x8D, 0x46, 0x10, 0x75

        // BetaAOB
        0x83, 0x7E, 0x14, 0xFF, 0x8D, 0x46, 0x14, 0x75

        // GammaAOB
        0x83, 0x7E, 0x18, 0xFF, 0x8D, 0x46, 0x18, 0x75

        // DeltaAOB
        0x83, 0x7E, 0x1C, 0xFF, 0x8D, 0x46, 0x1C, 0x75

        // EpsilonAOB
        0x83, 0x7E, 0x20, 0xFF, 0x8D, 0x46, 0x20, 0x75

        // ZetaAOB
        0x83, 0x7E, 0x24, 0xFF, 0x8D, 0x46, 0x24, 0x75

        // EtaAOB
        0x83, 0x7E, 0x28, 0xFF, 0x8D, 0x46, 0x28, 0x75

        // ThetaAOB
        0x83, 0x7E, 0x2C, 0xFF, 0x8D, 0x46, 0x2C, 0x75

        // MaterialAOB
        0x83, 0x7E, 0x30, 0xFF, 0x8D, 0x46, 0x30, 0x75
        */
        #endregion
        #region Blank Patterns
        /* 
        // AlphaAOB
        83 7E 10 FF 8D 46 10 75

        // BetaAOB
        83 7E 14 FF 8D 46 14 75

        // GammaAOB
        83 7E 18 FF 8D 46 18 75

        // DeltaAOB
        83 7E 1C FF 8D 46 1C 75

        // EpsilonAOB
        83 7E 20 FF 8D 46 20 75

        // ZetaAOB
        83 7E 24 FF 8D 46 24 75

        // EtaAOB
        83 7E 28 FF 8D 46 28 75

        // ThetaAOB
        83 7E 2C FF 8D 46 2C 75

        // MaterialAOB
        83 7E 30 FF 8D 46 30 75
        */
        #endregion
        #endregion

        //Patterns to looks for when the Bypass is disabled
        #region Disabled Patterns
        #region Patterns to use in code
        /*    
        // AlphaAOB
        0x83, 0x7E, 0x10, 0x00, 0x8D, 0x46, 0x10, 0x74

        // BetaAOB
        0x83, 0x7E, 0x14, 0xFF, 0x8D, 0x46, 0x14, 0x74

        // GammaAOB
        0x83, 0x7E, 0x18, 0x00, 0x8D, 0x46, 0x18, 0x74

        // DeltaAOB
        0x83, 0x7E, 0x1C, 0x00, 0x8D, 0x46, 0x1C, 0x74

        // EpsilonAOB
        0x83, 0x7E, 0x20, 0x00, 0x8D, 0x46, 0x20, 0x74 

        // ZetaAOB
        0x83, 0x7E, 0x24, 0x00, 0x8D, 0x46, 0x24, 0x74

        // EtaAOB
        0x83, 0x7E, 0x28, 0x00, 0x8D, 0x46, 0x28, 0x74

        // ThetaAOB
        0x83, 0x7E, 0x2C, 0x00, 0x8D, 0x46, 0x2C, 0x74

        // MaterialAOB
        0x83, 0x7E, 0x30, 0x00, 0x8D, 0x46, 0x30, 0x74
        */
        #endregion
        #region Blank Patterns
        /*    
        // AlphaAOB
        83 7E 10 00 8D 46 10 74

        // BetaAOB
        83 7E 14 FF 8D 46 14 74

        // GammaAOB
        83 7E 18 00 8D 46 18 74

        // DeltaAOB
        83 7E 1C 00 8D 46 1C 74

        // EpsilonAOB
        83 7E 20 00 8D 46 20 74 

        // ZetaAOB
        83 7E 24 00 8D 46 24 74

        // EtaAOB
        83 7E 28 00 8D 46 28 74

        // ThetaAOB
        83 7E 2C 00 8D 46 2C 74

        // MaterialAOB
        83 7E 30 00 8D 46 30 74
        */
        #endregion
        #endregion

        public static List<int> ItemSanityAddressList = new List<int>();
        public static bool CurrentItemBypass { get; set; }


        private static void GetDisabledPositionsItems()
        {
            ItemPattern.BytePatternListDisabledItems.Clear();
            ItemPattern.BytePatternListEnabledItems.Clear();
            var WP = new ItemPattern();
            foreach (var aob in ItemPattern.BytePatternListDisabledItems)
            {
                ItemSanityAddressList.AddRange(SearchPattern.SearchBytePattern(aob, File.ReadAllBytes(CheckPatched.BLPath)));
                if (ItemSanityAddressList.Count >= 1)
                    CurrentItemBypass = false;
            }
        }

        private static void GetEnabledPositionsItems()
        {
            //ItemonPattern.BytePatternListDisabled.Clear();
            //ItemonPattern.BytePatternListEnabled.Clear();
            ItemSanityAddressList.Clear();
            var WP = new ItemPattern();

            // For each Pattern, get the address
            foreach (var aob in ItemPattern.BytePatternListEnabledItems)
            {
                ItemSanityAddressList.AddRange(SearchPattern.SearchBytePattern(aob, File.ReadAllBytes(CheckPatched.BLPath)));
                // If all patterns were found, we have the address of all patterns and therefore the Itemon-Bypass is already enabled
                if (ItemSanityAddressList.Count >= 1)
                    CurrentItemBypass = true;
            }
        }

        public static void EnableItemBypass()
        {
            ItemSanityAddressList.Clear();
            GetDisabledPositionsItems();

            try
            {
                using (var stream = new FileStream(CheckPatched.BLPath, FileMode.Open, FileAccess.ReadWrite))
                {
                    foreach (var address in ItemSanityAddressList)
                    {
                        stream.Position = address + 3;
                        stream.WriteByte(0xFF);
                        stream.Position = address + 7;
                        stream.WriteByte(0x75);
                    }
                    stream.Close();
                }
                CurrentItemBypass = true;

            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("Could not find Borderlands2.exe!");
            }
            catch (IOException excep)
            {
                MessageBox.Show("Could not open Borderlands2.exe. Is it opened by another application?");
            }
            catch (Exception excep)
            {
                MessageBox.Show("Something went wrong!\n{0}", excep.Message);
            }
            finally
            {
                GC.Collect();
            }
        }

        public static void DisableItemBypass()
        {
            ItemSanityAddressList.Clear();
            GetEnabledPositionsItems();

            try
            {
                using (var stream = new FileStream(CheckPatched.BLPath, FileMode.Open, FileAccess.ReadWrite))
                {
                    foreach (var address in ItemSanityAddressList)
                    {
                        stream.Position = address + 3;
                        stream.WriteByte(0x00);
                        stream.Position = address + 7;
                        stream.WriteByte(0x74);
                    }
                    stream.Close();
                }
                CurrentItemBypass = false;

            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("Could not find Borderlands2.exe!");
            }
            catch (IOException excep)
            {
                MessageBox.Show("Could not open Borderlands2.exe. Is it opened by another application?");
            }
            catch (Exception excep)
            {
                MessageBox.Show("Something went wrong!\n{0}", excep.Message);
            }
            finally
            {
                GC.Collect();
            }
        }

        public static bool ItemSanityStatus()
        {
            GetEnabledPositionsItems();
            return CurrentItemBypass;
        }

    }

    public class ItemPattern
    {
        public static List<byte[]> BytePatternListDisabledItems = new List<byte[]>();
        public static List<byte[]> BytePatternListEnabledItems = new List<byte[]>();

        public ItemPattern()
        {
            #region Add all "Disabled" Patterns to DisabledPatternList
            // AlphaCheck
            PatternBytesDisabledItems(new byte[] { 0x83, 0x7E, 0x10, 0x00, 0x8D, 0x46, 0x10, 0x74 });
            // BetaCheck
            PatternBytesDisabledItems(new byte[] { 0x83, 0x7E, 0x14, 0xFF, 0x8D, 0x46, 0x14, 0x74 });
            // GammaCheck
            PatternBytesDisabledItems(new byte[] { 0x83, 0x7E, 0x18, 0x00, 0x8D, 0x46, 0x18, 0x74 });
            // DeltaCheck
            PatternBytesDisabledItems(new byte[] { 0x83, 0x7E, 0x1C, 0x00, 0x8D, 0x46, 0x1C, 0x74 });
            // EpsilonCheck
            PatternBytesDisabledItems(new byte[] { 0x83, 0x7E, 0x20, 0x00, 0x8D, 0x46, 0x20, 0x74 });
            // ZetaCheck
            PatternBytesDisabledItems(new byte[] { 0x83, 0x7E, 0x24, 0x00, 0x8D, 0x46, 0x24, 0x74 });
            // EtaCheck
            PatternBytesDisabledItems(new byte[] { 0x83, 0x7E, 0x28, 0x00, 0x8D, 0x46, 0x28, 0x74 });
            // ThetaCheck
            PatternBytesDisabledItems(new byte[] { 0x83, 0x7E, 0x2C, 0x00, 0x8D, 0x46, 0x2C, 0x74 });
            // MaterialCheck
            PatternBytesDisabledItems(new byte[] { 0x83, 0x7E, 0x30, 0x00, 0x8D, 0x46, 0x30, 0x74 });
            #endregion

            #region Add all "Enabled" Patterns to EnabledPatternListItems
            // AlphaCheck
            PatternBytesEnabledItems(new byte[] { 0x83, 0x7E, 0x10, 0xFF, 0x8D, 0x46, 0x10, 0x75 });
            // BetaCheck
            PatternBytesEnabledItems(new byte[] { 0x83, 0x7E, 0x14, 0xFF, 0x8D, 0x46, 0x14, 0x75 });
            // GammaCheck
            PatternBytesEnabledItems(new byte[] { 0x83, 0x7E, 0x18, 0xFF, 0x8D, 0x46, 0x18, 0x75 });
            // DeltaCheck
            PatternBytesEnabledItems(new byte[] { 0x83, 0x7E, 0x1C, 0xFF, 0x8D, 0x46, 0x1C, 0x75 });
            // EpsilonCheck
            PatternBytesEnabledItems(new byte[] { 0x83, 0x7E, 0x20, 0xFF, 0x8D, 0x46, 0x20, 0x75 });
            // ZetaCheck
            PatternBytesEnabledItems(new byte[] { 0x83, 0x7E, 0x24, 0xFF, 0x8D, 0x46, 0x24, 0x75 });
            // EtaCheck
            PatternBytesEnabledItems(new byte[] { 0x83, 0x7E, 0x28, 0xFF, 0x8D, 0x46, 0x28, 0x75 });
            // ThetaCheck
            PatternBytesEnabledItems(new byte[] { 0x83, 0x7E, 0x2C, 0xFF, 0x8D, 0x46, 0x2C, 0x75 });
            // MaterialCheck
            PatternBytesEnabledItems(new byte[] { 0x83, 0x7E, 0x30, 0xFF, 0x8D, 0x46, 0x30, 0x75 });
            #endregion
        }

        /// <summary>
        /// Adding the patterns to the DisabledPatternListItems
        /// </summary>
        /// <param name="patternBytes">Array of bytes to add to the Disabled PatternList</param>
        public static void PatternBytesDisabledItems(byte[] patternBytes)
        {
            BytePatternListDisabledItems.Add(patternBytes);
        }

        /// <summary>
        /// Adding the patterns to the EnabledPatternListItems
        /// </summary>
        /// <param name="patternBytes">Array of bytes to add to the Enabled PatternList</param>
        public static void PatternBytesEnabledItems(byte[] patternBytes)
        {
            BytePatternListEnabledItems.Add(patternBytes);
        }
    }

}

