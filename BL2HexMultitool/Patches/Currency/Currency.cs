﻿/*
					Copyright 2017 c0dycode

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BL2HexMultitool
{
    public partial class Eridium : Form
    {
        #region Public Fields
        public int IntMaxCash { get; set; }
        public int TempCashAddress { get; set; }

        public int IntMaxEridium { get; set; }
        public int TempEridAddress { get; set; }

        public int IntMaxSeraph { get; set; }
        public int TempSeraphAddress { get; set; }

        public int IntMaxTorque { get; set; }
        public int TempTorqueAddress { get; set; }
#endregion

        //Currencies.Targets Target;
        public Currencies Currencies = new Currencies();

        public Eridium()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Save();
            this.Close();
        }

        private void Eridium_Load(object sender, EventArgs e)
        {
            //byte[] checker = { 0x60, 0xB1, 0xD7, 0x00, 0x20, 0x16, 0x00, 0x01, 0x60, 0x3A, 0x45, 0x00, 0xFF, 0xE0, 0xF5, 0x05 };
            // All patterns can be found in ../Helpers/SearchPattern.cs
            // -20 = Max Cash, -16 = Max Eridium, -12 = Max Seraph, -8 = Unknown, -4 = Torque
            TempCashAddress = CheckPatched.ScanForPatternAddress(SearchPattern.EridiumPattern, -20);
            TempEridAddress = CheckPatched.ScanForPatternAddress(SearchPattern.EridiumPattern, -16);
            TempSeraphAddress = CheckPatched.ScanForPatternAddress(SearchPattern.EridiumPattern, -12);
            TempTorqueAddress = CheckPatched.ScanForPatternAddress(SearchPattern.EridiumPattern, -4);
            
            if (TempEridAddress > 2)
            {
                try
                {
                    // Read Max Eridium
                    numericUpDownMaxEridium.Value = Currencies.GetCurrency(TempEridAddress);
                    // Read Max Eridium
                    numericUpDownMaxSeraph.Value = Currencies.GetCurrency(TempSeraphAddress);
                    // Read Max Eridium
                    numericUpDownMaxTorque.Value = Currencies.GetCurrency(TempTorqueAddress);
                    // Read Max Eridium
                    numericUpDownMaxCash.Value = Currencies.GetCurrency(TempCashAddress);

                    //using (var stream = new FileStream(CheckPatched.BLPath, FileMode.Open, FileAccess.ReadWrite))
                    //{
                    //    List<byte> eridlist = new List<byte>();
                    //    stream.Position = TempEridAddress;
                    //    for (int i = 0; i < 4; i++)
                    //    {
                    //        int eridvalue = stream.ReadByte();
                    //        byte beridvalue = Convert.ToByte(eridvalue);
                    //        eridlist.Add(beridvalue);
                    //    }
                    //    byte[] temparray = eridlist.ToArray();
                    //    int finaleridvalue = BitConverter.ToInt32(temparray, 0);
                    //    CurrencyEridium.MaxEridium = finaleridvalue;
                    //MessageBox.Show(finaleridvalue.ToString());
                }

                catch (FileNotFoundException)
                {
                    MessageBox.Show("Could not find Borderlands2.exe!");
                }
                catch (IOException excep)
                {
                    MessageBox.Show("Could not open Borderlands2.exe. Is it opened by another application?");
                }
                catch (Exception excep)
                {
                    MessageBox.Show("Something went wrong!\n{0}", excep.Message);
                }
                finally
                {
                    GC.Collect();
                }
            }
            else
                this.Close();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                Currencies.SetCurrency(TempCashAddress, Currencies.Targets.Cash);
                Currencies.SetCurrency(TempEridAddress, Currencies.Targets.Eridium);
                Currencies.SetCurrency(TempSeraphAddress, Currencies.Targets.Seraph);
                Currencies.SetCurrency(TempTorqueAddress, Currencies.Targets.Torque);

                //using (var stream = new FileStream(CheckPatched.BLPath, FileMode.Open, FileAccess.ReadWrite))
                //{
                //    byte[] finalbytes = BitConverter.GetBytes(CurrencyEridium.MaxEridium);

                //    stream.Position = TempEridAddress;
                //    //MessageBox.Show(BitConverter.ToString(finalbytes));
                //    stream.Write(finalbytes, 0, 4);
                //}
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("Could not find Borderlands2.exe!");
            }
            catch (IOException excep)
            {
                MessageBox.Show("Could not open Borderlands2.exe. Is it opened by another application?");
            }
            catch (Exception excep)
            {
                MessageBox.Show("Something went wrong!\n{0}", excep.Message);
            }
            finally
            {
                MessageBox.Show("Patch has been applied!");
                GC.Collect();
            }
        }

        private void Eridium_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.Save();
        }

        private void numericUpDownMaxEridium_ValueChanged(object sender, EventArgs e)
        {
            Currencies.MaxEridium = Convert.ToInt32(numericUpDownMaxEridium.Value);
        }

        private void numericUpDownMaxTorque_ValueChanged(object sender, EventArgs e)
        {
            Currencies.MaxTorque = Convert.ToInt32(numericUpDownMaxTorque.Value);
        }

        private void numericUpDownMaxSeraph_ValueChanged(object sender, EventArgs e)
        {
            Currencies.MaxSeraph = Convert.ToInt32(numericUpDownMaxSeraph.Value);
        }

        private void numericUpDownMaxCash_ValueChanged(object sender, EventArgs e)
        {
            Currencies.MaxCash = Convert.ToInt32(numericUpDownMaxCash.Value);
        }
    }
}
