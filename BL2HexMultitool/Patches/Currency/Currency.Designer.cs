﻿using System.Drawing;
using System.Windows.Forms;

namespace BL2HexMultitool
{
    partial class Eridium
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Eridium));
            this.lblMaxErid = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.LblCurrencyPatching = new System.Windows.Forms.Label();
            this.numericUpDownMaxEridium = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownMaxTorque = new System.Windows.Forms.NumericUpDown();
            this.LblTorque = new System.Windows.Forms.Label();
            this.numericUpDownMaxSeraph = new System.Windows.Forms.NumericUpDown();
            this.LblSeraph = new System.Windows.Forms.Label();
            this.numericUpDownMaxCash = new System.Windows.Forms.NumericUpDown();
            this.LblMoney = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxEridium)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxTorque)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxSeraph)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxCash)).BeginInit();
            this.SuspendLayout();
            // 
            // lblMaxErid
            // 
            this.lblMaxErid.AutoSize = true;
            this.lblMaxErid.Location = new System.Drawing.Point(29, 43);
            this.lblMaxErid.Name = "lblMaxErid";
            this.lblMaxErid.Size = new System.Drawing.Size(64, 13);
            this.lblMaxErid.TabIndex = 9;
            this.lblMaxErid.Text = "Max Eridium";
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(132, 85);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.BackColor = Color.FromArgb(40, 40, 40);
            this.btnClose.ForeColor = Color.White;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnApply
            // 
            this.btnApply.Location = new System.Drawing.Point(32, 85);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(75, 23);
            this.btnApply.TabIndex = 7;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.BackColor = Color.FromArgb(40, 40, 40);
            this.btnApply.ForeColor = Color.White;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // LblCurrencyPatching
            // 
            this.LblCurrencyPatching.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LblCurrencyPatching.AutoSize = true;
            this.LblCurrencyPatching.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCurrencyPatching.Location = new System.Drawing.Point(183, 21);
            this.LblCurrencyPatching.Name = "LblCurrencyPatching";
            this.LblCurrencyPatching.Size = new System.Drawing.Size(111, 13);
            this.LblCurrencyPatching.TabIndex = 5;
            this.LblCurrencyPatching.Text = "Currency-Patching";
            // 
            // numericUpDownMaxEridium
            // 
            this.numericUpDownMaxEridium.Location = new System.Drawing.Point(32, 59);
            this.numericUpDownMaxEridium.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.numericUpDownMaxEridium.Minimum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numericUpDownMaxEridium.Name = "numericUpDownMaxEridium";
            this.numericUpDownMaxEridium.Size = new System.Drawing.Size(84, 20);
            this.numericUpDownMaxEridium.TabIndex = 10;
            this.numericUpDownMaxEridium.TextAlign = HorizontalAlignment.Right;
            this.numericUpDownMaxEridium.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numericUpDownMaxEridium.BackColor = Color.FromArgb(40, 40, 40);
            this.numericUpDownMaxEridium.ForeColor = Color.White;
            this.numericUpDownMaxEridium.ValueChanged += new System.EventHandler(this.numericUpDownMaxEridium_ValueChanged);
            // 
            // numericUpDownMaxTorque
            // 
            this.numericUpDownMaxTorque.Location = new System.Drawing.Point(132, 59);
            this.numericUpDownMaxTorque.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.numericUpDownMaxTorque.Minimum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numericUpDownMaxTorque.Name = "numericUpDownMaxTorque";
            this.numericUpDownMaxTorque.Size = new System.Drawing.Size(84, 20);
            this.numericUpDownMaxTorque.TabIndex = 12;
            this.numericUpDownMaxTorque.TextAlign = HorizontalAlignment.Right;
            this.numericUpDownMaxTorque.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numericUpDownMaxTorque.BackColor = Color.FromArgb(40, 40, 40);
            this.numericUpDownMaxTorque.ForeColor = Color.White;
            this.numericUpDownMaxTorque.ValueChanged += new System.EventHandler(this.numericUpDownMaxTorque_ValueChanged);
            // 
            // LblTorque
            // 
            this.LblTorque.AutoSize = true;
            this.LblTorque.Location = new System.Drawing.Point(129, 43);
            this.LblTorque.Name = "LblTorque";
            this.LblTorque.Size = new System.Drawing.Size(64, 13);
            this.LblTorque.TabIndex = 11;
            this.LblTorque.Text = "Max Torque";
            // 
            // numericUpDownMaxSeraph
            // 
            this.numericUpDownMaxSeraph.Location = new System.Drawing.Point(237, 59);
            this.numericUpDownMaxSeraph.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.numericUpDownMaxSeraph.Minimum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numericUpDownMaxSeraph.Name = "numericUpDownMaxSeraph";
            this.numericUpDownMaxSeraph.Size = new System.Drawing.Size(84, 20);
            this.numericUpDownMaxSeraph.TabIndex = 14;
            this.numericUpDownMaxSeraph.TextAlign = HorizontalAlignment.Right;
            this.numericUpDownMaxSeraph.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numericUpDownMaxSeraph.BackColor = Color.FromArgb(40, 40, 40);
            this.numericUpDownMaxSeraph.ForeColor = Color.White;
            this.numericUpDownMaxSeraph.ValueChanged += new System.EventHandler(this.numericUpDownMaxSeraph_ValueChanged);
            // 
            // LblSeraph
            // 
            this.LblSeraph.AutoSize = true;
            this.LblSeraph.Location = new System.Drawing.Point(234, 43);
            this.LblSeraph.Name = "LblSeraph";
            this.LblSeraph.Size = new System.Drawing.Size(64, 13);
            this.LblSeraph.TabIndex = 13;
            this.LblSeraph.Text = "Max Seraph";
            // 
            // numericUpDownMaxCash
            // 
            this.numericUpDownMaxCash.Location = new System.Drawing.Point(345, 59);
            this.numericUpDownMaxCash.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.numericUpDownMaxCash.Minimum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numericUpDownMaxCash.Name = "numericUpDownMaxCash";
            this.numericUpDownMaxCash.Size = new System.Drawing.Size(84, 20);
            this.numericUpDownMaxCash.TabIndex = 16;
            this.numericUpDownMaxCash.TextAlign = HorizontalAlignment.Right;
            this.numericUpDownMaxCash.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numericUpDownMaxCash.BackColor = Color.FromArgb(40, 40, 40);
            this.numericUpDownMaxCash.ForeColor = Color.White;
            this.numericUpDownMaxCash.ValueChanged += new System.EventHandler(this.numericUpDownMaxCash_ValueChanged);
            // 
            // LblMoney
            // 
            this.LblMoney.AutoSize = true;
            this.LblMoney.Location = new System.Drawing.Point(342, 43);
            this.LblMoney.Name = "LblMoney";
            this.LblMoney.Size = new System.Drawing.Size(62, 13);
            this.LblMoney.TabIndex = 15;
            this.LblMoney.Text = "Max Money";
            // 
            // MaxInfoLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(300, 102);
            this.label1.Name = "maxInfo";
            this.label1.Size = new System.Drawing.Size(169, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Maximum for each is: 2147483647";
            // 
            // Currency
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = Color.FromArgb(40,40,40);
            this.ForeColor = Color.White;
            this.ClientSize = new System.Drawing.Size(469, 117);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numericUpDownMaxCash);
            this.Controls.Add(this.LblMoney);
            this.Controls.Add(this.numericUpDownMaxSeraph);
            this.Controls.Add(this.LblSeraph);
            this.Controls.Add(this.numericUpDownMaxTorque);
            this.Controls.Add(this.LblTorque);
            this.Controls.Add(this.numericUpDownMaxEridium);
            this.Controls.Add(this.lblMaxErid);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.LblCurrencyPatching);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Eridium";
            this.Text = "Currency Patches";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Eridium_FormClosing);
            this.Load += new System.EventHandler(this.Eridium_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxEridium)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxTorque)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxSeraph)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxCash)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMaxErid;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Label LblCurrencyPatching;
        private System.Windows.Forms.NumericUpDown numericUpDownMaxEridium;
        private System.Windows.Forms.NumericUpDown numericUpDownMaxTorque;
        private System.Windows.Forms.Label LblTorque;
        private System.Windows.Forms.NumericUpDown numericUpDownMaxSeraph;
        private System.Windows.Forms.Label LblSeraph;
        private System.Windows.Forms.NumericUpDown numericUpDownMaxCash;
        private System.Windows.Forms.Label LblMoney;
        private System.Windows.Forms.Label label1;
    }
}