﻿/*
					Copyright 2017 c0dycode

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BL2HexMultitool
{
    public partial class BackPack : Form
    {
        public int CurrentSpace { get; set; }
        public Backpack BP = new Backpack();

        public BackPack()
        {
            InitializeComponent();
        }

        private void BackPack_Load(object sender, EventArgs e)
        {
            // Scanpatterns for the various backpack-patches
            //Stock Pattern
            byte[] pattern = {204, 204, 139, 129, 184, 1, 0, 0, 195};
            //Version 1 Pattern
            byte[] checkv1 = {204, 204, 139, 129, 184, 1, 0, 0, 131, 192};
            //Version 2 Pattern
            byte[] checkv2 = {204, 204, 139, 129, 184, 1, 0, 0, 131, 248};

            //Version 3 Pattern
            // This is the latest version and the most stable one
            // I've continued to call this the "Testpatch"
            // This allows for way more Backpack-Space than all the other 'hacky' methods
            //byte[] checkv3 = {0x0C, 0x00, 0x00, 0x00, 0xEB, 0x0A, 0x83, 0xF8};

            CheckPatched.CheckPatch(pattern);

            int currentaddress = Properties.Settings.Default.address;
            GC.Collect();

            //Check if not patched before
            if (currentaddress.ToString().Length <= 4)
            {
                GC.Collect();
                //Check V1-Patch
                CheckPatched.CheckPatch(checkv1);

                currentaddress = Properties.Settings.Default.address;
                if (currentaddress.ToString().Length <= 4)
                {
                    GC.Collect();
                    //Check V1-Patch
                    CheckPatched.CheckPatch(checkv2);
                    currentaddress = Properties.Settings.Default.address;
                }
            }
            int testpatchaddress = Properties.Settings.Default.testpatchaddress;
            CheckPatched.CheckTestPatch(SearchPattern.BackpackPattern);
            try
            {
                using (var stream = new FileStream(CheckPatched.BLPath, FileMode.Open, FileAccess.ReadWrite))
                {
                    stream.Position = Properties.Settings.Default.address + 9;
                    int patchstatus = stream.ReadByte();

                    if (patchstatus == 204)
                    {
                        /*var patchdecision = MessageBox.Show("Would you like to apply the Patch now?\nDo not use it if you want to use the current Test-Patch!", "Backpack-Patch not applied", MessageBoxButtons.YesNo);
                        if (patchdecision == DialogResult.Yes)
                        {
                            stream.Close();
                            CheckPatched.ApplyDefPatch();
                            checkBoxEnable.Checked = true;
                        }
                        else if (patchdecision == DialogResult.No)
                        {*/
                        stream.Close();
                        checkBoxEnable.Checked = false;
                        btnApply.Text = "Patch";
                        numericUDTrigger.Enabled = false;
                        numericSlotNumber.Enabled = false;
                        //}
                    }
                    else if (patchstatus == 195)
                    {

                        stream.Close();
                        checkBoxEnable.Checked = false;
                        btnApply.Text = "Patch";
                        numericUDTrigger.Enabled = false;
                        numericSlotNumber.Enabled = false;
                        /*var patchdecision = MessageBox.Show("This tool only works with the newest version. \n " +
                        "Would you like to apply the Patch now?", "Old Backpack-Patch found", MessageBoxButtons.YesNo);
                        if (patchdecision == DialogResult.Yes)
                        {
                            stream.Close();
                            CheckPatched.ApplyDefPatch();
                            checkBoxEnable.Checked = true;
                        }
                        else if (patchdecision == DialogResult.No)
                        {
                            stream.Close();
                            checkBoxEnable.Checked = false;
                            btnApply.Text = "Patch";
                            numericUDTrigger.Enabled = false;
                            numericSlotNumber.Enabled = false;
                        }*/
                    }
                    else if (patchstatus == 125)
                    {
                        stream.Close();
                        checkBoxEnable.Checked = true;
                    }

                    lblAmount.Text = "Enter Max Slots Min 12 | Max 255";

                    numericSlotNumber.Minimum = 0;
                    numericSlotNumber.Maximum = 255;
                    btnApply.Text = "Apply";

                    numericUDTrigger.Enabled = true;
                    numericSlotNumber.Enabled = true;
                    checkBoxTestpatch.Checked = true;
                    checkBoxTestpatch.Enabled = false;
                    checkBoxEnable.Visible = false;

                    /*stream.Position = Properties.Settings.Default.testpatchaddress;
                    var TestTriggerValue = stream.ReadByte();
                    numericUDTrigger.Value = TestTriggerValue;
                    stream.Position = Properties.Settings.Default.testpatchaddress + 4;
                    var BPValue = stream.ReadByte();
                    numericSlotNumber.Value = BPValue;*/
                }
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("Could not find Borderlands2.exe!");
            }
            catch (IOException excep)
            {
                MessageBox.Show("Could not open Borderlands2.exe. Is it opened by another application?");
            }
            catch (Exception excep)
            {
                MessageBox.Show("Something went wrong!\n{0}", excep.Message);
            }
            finally
            {
                GC.Collect();
            }

            try
            {
                using (var stream = new FileStream(CheckPatched.BLPath, FileMode.Open, FileAccess.ReadWrite))
                {
                    if (checkBoxTestpatch.Checked == false)
                    {
                        stream.Position = Properties.Settings.Default.address + 8;
                        BP.Space = stream.ReadByte();
                        if (BP.Space > 39)
                        {
                            numericUDTrigger.Value = 39;
                            numericUDTrigger.Enabled = false;
                        }
                        else
                            numericUDTrigger.Value = BP.Space;

                        stream.Position = Properties.Settings.Default.address + 14;
                        int currentnumber = stream.ReadByte();
                        if (currentnumber > 127)
                            numericSlotNumber.Value = 0;
                        else
                            numericSlotNumber.Value = currentnumber;
                    }
                    else if (checkBoxTestpatch.Checked == true)
                    {
                        stream.Position = Properties.Settings.Default.testpatchaddress;
                        BP.Space = stream.ReadByte();
                        if (BP.Space > 39)
                        {
                            numericUDTrigger.Value = 39;
                            numericUDTrigger.Enabled = false;
                        }
                        else
                            numericUDTrigger.Value = BP.Space;

                        stream.Position = Properties.Settings.Default.testpatchaddress + 4;
                        int currentnumber = stream.ReadByte();
                        if (currentnumber > 255)
                            numericSlotNumber.Value = 0;
                        else
                            numericSlotNumber.Value = currentnumber;
                    }

                }
            }
            catch (FileNotFoundException excep)
            {
                MessageBox.Show("Could not find \n{0}", excep.FileName);
            }
            catch (Exception excep)
            {
                MessageBox.Show("Something went wrong!\n{0}", excep.Message);
            }
            finally
            {
                //Clear the ByteArray to remove the BL2 exe from memory
                GC.Collect();
            }
        }


        private void numericSlotNumber_TextChanged(object sender, EventArgs e)
        {

            if (checkBoxTestpatch.Checked == true)
            {
                BP.Space = Convert.ToInt32(Math.Round(numericSlotNumber.Value, 0));
                lblTotalCount.Text = numericSlotNumber.Value.ToString();
                //string HexValue = BP.Space.ToString("X");
                //BP.Space = CurrentValue;
            }
        }

        private void numericSlotNumber_ValueChanged(object sender, EventArgs e)
        {

            if (checkBoxTestpatch.Checked == false)
            {
                int SlotBaseMaxValue = 39;
                BP.Space = Convert.ToInt32(Math.Round(numericSlotNumber.Value, 0));
                lblTotalCount.Text = Convert.ToString(BP.Space + SlotBaseMaxValue);
                //string HexValue = CurrentValue.ToString("X");
                //BP.Space = CurrentValue;
            }
            else if (checkBoxTestpatch.Checked == true)
            {
                BP.Space = Convert.ToInt32(Math.Round(numericSlotNumber.Value, 0));
                lblTotalCount.Text = numericSlotNumber.Value.ToString();
                //string HexValue = CurrentValue.ToString("X");
                //BP.Space = CurrentValue;
            }
            GC.Collect();
        }

        private void numericSlotNumber_UpdateValue(object sender, EventArgs e)
        {
            lblTotalCount.Text = BP.Space.ToString();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Save();
            this.Close();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            if (checkBoxEnable.Checked && btnApply.Text == "Apply" && checkBoxTestpatch.Checked == false)
            {
                try
                {
                    using (var stream = new FileStream(CheckPatched.BLPath, FileMode.Open, FileAccess.ReadWrite))
                    {
                        BinaryWriter writeBinary = new BinaryWriter(stream);
                        BP.Spacetrigger = Convert.ToInt16(Math.Round(numericUDTrigger.Value, 0));
                        byte FinalCurrentTriggerValue = Convert.ToByte(BP.Spacetrigger);
                        stream.Position = Properties.Settings.Default.address + 8;
                        stream.WriteByte(FinalCurrentTriggerValue);
                        stream.Position = Properties.Settings.Default.address + 14;
                        BP.Space = Convert.ToInt16(Math.Round(numericSlotNumber.Value, 0));

                        if (BP.Space < 12)
                        {
                            BP.Space = 12;
                            numericSlotNumber.Value = 12;
                            lblTotalCount.Text = 12.ToString();
                        }
                        sbyte finalvalue = Convert.ToSByte(BP.Space);
                        writeBinary.Write(finalvalue);
                        stream.Close();
                    }
                }
                catch (FileNotFoundException excep)
                {
                    MessageBox.Show("Could not find \n{0}", excep.FileName);
                }
                catch (Exception excep)
                {
                    MessageBox.Show("Something went wrong!\n{0}", excep.Message);
                }
                finally
                {
                    //Clear the ByteArray to remove the BL2 exe from memory
                    GC.Collect();
                }
                MessageBox.Show("Successfully saved changes.");
            }
            else if (checkBoxTestpatch.Checked == true)
            {
                if (Properties.Settings.Default.patched == "true")
                {
                    var TestPatchDecision = MessageBox.Show("This will remove previous patches!", "Try the Test-Patch?",
                        MessageBoxButtons.YesNo);
                    if (TestPatchDecision == DialogResult.Yes)
                    {
                        CheckPatched.RevertPatch();
                    }
                }
                CheckPatched.RevertPatch();
                try
                {
                    using (var stream = new FileStream(CheckPatched.BLPath, FileMode.Open, FileAccess.ReadWrite))
                    {
                        //BinaryWriter writeBinary = new BinaryWriter(stream);
                        BP.Spacetrigger = Convert.ToInt16(Math.Round(numericUDTrigger.Value, 0));
                        byte FinalCurrentTriggerValue = Convert.ToByte(BP.Spacetrigger);
                        stream.Position = Properties.Settings.Default.testpatchaddress;
                        stream.WriteByte(FinalCurrentTriggerValue);
                        stream.Position = Properties.Settings.Default.testpatchaddress + 4;
                        BP.Space = Convert.ToInt16(Math.Round(numericSlotNumber.Value, 0));
                        if (BP.Space < 12)
                        {
                            BP.Space = 12;
                            numericSlotNumber.Value = 12;
                            lblTotalCount.Text = 12.ToString();
                        }

                        //byte finalvalue = Convert.ToByte(BP.Space);
                        var checkGetBytes = BitConverter.GetBytes(BP.Space);
                        //writeBinary.Write(finalvalue);
                        stream.Write(checkGetBytes, 0, 4);
                        stream.Close();
                    }
                }
                catch (FileNotFoundException excep)
                {
                    MessageBox.Show("Could not find \n{0}", excep.FileName);
                }
                catch (Exception excep)
                {
                    MessageBox.Show("Something went wrong!\n{0}", excep.Message);
                }
                finally
                {
                    GC.Collect();
                }
                MessageBox.Show("Successfully saved changes.");
            }

            else if (btnApply.Text == "Patch")
            {
                CheckPatched.ApplyDefPatch();
                MessageBox.Show("Successfully applied the patch! \nEnjoy");
                btnApply.Text = "Apply";
                checkBoxEnable.Checked = true;
                numericUDTrigger.Enabled = true;
                numericSlotNumber.Enabled = true;
            }
            GC.Collect();
        }

        private void BackPack_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.Save();
        }

        private void numericSlotNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (checkBoxTestpatch.Checked == false)
            {
                int SlotBaseMaxValue = 39;
                int CurrentValue = Convert.ToInt32(Math.Round(numericSlotNumber.Value, 0));
                lblTotalCount.Text = Convert.ToString(CurrentValue + SlotBaseMaxValue);
                string HexValue = CurrentValue.ToString("X");
            }
            else if (checkBoxTestpatch.Checked == true)
            {
                int CurrentValue = Convert.ToInt32(Math.Round(numericSlotNumber.Value, 0));
                lblTotalCount.Text = numericSlotNumber.Value.ToString();
                string HexValue = CurrentValue.ToString("X");
            }
            GC.Collect();
        }

        private void numericSlotNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if (checkBoxTestpatch.Checked == false)
            {
                int SlotBaseMaxValue = 39;
                int CurrentValue = Convert.ToInt32(Math.Round(numericSlotNumber.Value, 0));
                lblTotalCount.Text = Convert.ToString(CurrentValue + SlotBaseMaxValue);
                string HexValue = CurrentValue.ToString("X");
            }
            else if (checkBoxTestpatch.Checked == true)
            {
                int CurrentValue = Convert.ToInt32(Math.Round(numericSlotNumber.Value, 0));
                lblTotalCount.Text = numericSlotNumber.Value.ToString();
                string HexValue = CurrentValue.ToString("X");
            }

            GC.Collect();
        }

        private void btnRemPatch_Click(object sender, EventArgs e)
        {
            var RemPatchDecision = MessageBox.Show("Do you really want to remove the Backpack-Patch?",
                "Restore default Backpack?", MessageBoxButtons.YesNo);
            if (RemPatchDecision == DialogResult.Yes)
            {
                // Force removal of any previous patches from old versions/manual patching
                CheckPatched.RevertPatch();
                numericUDTrigger.Value = 39;
                numericSlotNumber.Value = 39;
            }
        }

        private void checkBoxTestpatch_CheckStateChanged(object sender, EventArgs e)
        {
            if (checkBoxTestpatch.Checked == true)
            {
                lblAmount.Text = "Enter Max Slots Min 12 | Max 255";

                numericSlotNumber.Minimum = 0;
                numericSlotNumber.Maximum = 255;
                btnApply.Text = "Apply";
                checkBoxEnable.Checked = false;
                numericUDTrigger.Enabled = true;
                numericSlotNumber.Enabled = true;
            }
            else if (checkBoxTestpatch.Checked == false && Properties.Settings.Default.patched == "false")
            {
                lblAmount.Text = "Slots to add (min. -127 /max. 127)";
                numericSlotNumber.Minimum = -127;
                numericSlotNumber.Maximum = 127;
                btnApply.Text = "Patch";
                checkBoxEnable.Checked = false;
                numericUDTrigger.Enabled = true;
                numericSlotNumber.Enabled = true;
            }
            else if (checkBoxTestpatch.Checked == false)
            {
                lblAmount.Text = "Slots to add (min. -127 /max. 127)";
                numericSlotNumber.Minimum = -127;
                numericSlotNumber.Maximum = 127;
                btnApply.Text = "Apply";
                checkBoxEnable.Checked = false;
                numericUDTrigger.Enabled = true;
                numericSlotNumber.Enabled = true;
            }
        }
    }
}
