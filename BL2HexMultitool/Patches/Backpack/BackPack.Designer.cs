﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace BL2HexMultitool
{
    partial class BackPack
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BackPack));
            this.btnRemPatch = new System.Windows.Forms.Button();
            this.lblSpaceTrigger = new System.Windows.Forms.Label();
            this.numericUDTrigger = new System.Windows.Forms.NumericUpDown();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.lblTotalCount = new System.Windows.Forms.Label();
            this.lblTotalSlots = new System.Windows.Forms.Label();
            this.lblAmount = new System.Windows.Forms.Label();
            this.numericSlotNumber = new System.Windows.Forms.NumericUpDown();
            this.checkBoxEnable = new System.Windows.Forms.CheckBox();
            this.checkBoxTestpatch = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUDTrigger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericSlotNumber)).BeginInit();
            this.SuspendLayout();
            // 
            // btnRemPatch
            // 
            this.btnRemPatch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnRemPatch.ForeColor = System.Drawing.Color.White;
            this.btnRemPatch.Location = new System.Drawing.Point(93, 96);
            this.btnRemPatch.Name = "btnRemPatch";
            this.btnRemPatch.Size = new System.Drawing.Size(88, 23);
            this.btnRemPatch.TabIndex = 22;
            this.btnRemPatch.Text = "Remove Patch";
            this.btnRemPatch.UseVisualStyleBackColor = false;
            this.btnRemPatch.Click += new System.EventHandler(this.btnRemPatch_Click);
            // 
            // lblSpaceTrigger
            // 
            this.lblSpaceTrigger.AutoSize = true;
            this.lblSpaceTrigger.Location = new System.Drawing.Point(10, 48);
            this.lblSpaceTrigger.Name = "lblSpaceTrigger";
            this.lblSpaceTrigger.Size = new System.Drawing.Size(67, 13);
            this.lblSpaceTrigger.TabIndex = 21;
            this.lblSpaceTrigger.Text = "Spacetrigger";
            // 
            // numericUDTrigger
            // 
            this.numericUDTrigger.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.numericUDTrigger.ForeColor = System.Drawing.Color.White;
            this.numericUDTrigger.TextAlign = HorizontalAlignment.Right;
            this.numericUDTrigger.Location = new System.Drawing.Point(13, 68);
            this.numericUDTrigger.Maximum = new decimal(new int[] {
            39,
            0,
            0,
            0});
            this.numericUDTrigger.Minimum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.numericUDTrigger.Name = "numericUDTrigger";
            this.numericUDTrigger.Size = new System.Drawing.Size(59, 20);
            this.numericUDTrigger.TabIndex = 20;
            this.numericUDTrigger.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(187, 96);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 19;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnApply
            // 
            this.btnApply.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnApply.ForeColor = System.Drawing.Color.White;
            this.btnApply.Location = new System.Drawing.Point(12, 96);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(75, 23);
            this.btnApply.TabIndex = 18;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = false;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // lblTotalCount
            // 
            this.lblTotalCount.AutoSize = true;
            this.lblTotalCount.Location = new System.Drawing.Point(208, 60);
            this.lblTotalCount.Name = "lblTotalCount";
            this.lblTotalCount.Size = new System.Drawing.Size(19, 13);
            this.lblTotalCount.TabIndex = 17;
            this.lblTotalCount.Text = "39";
            // 
            // lblTotalSlots
            // 
            this.lblTotalSlots.AutoSize = true;
            this.lblTotalSlots.Location = new System.Drawing.Point(189, 40);
            this.lblTotalSlots.Name = "lblTotalSlots";
            this.lblTotalSlots.Size = new System.Drawing.Size(53, 13);
            this.lblTotalSlots.TabIndex = 16;
            this.lblTotalSlots.Text = "Max Slots";
            // 
            // lblAmount
            // 
            this.lblAmount.AutoSize = true;
            this.lblAmount.Location = new System.Drawing.Point(10, 5);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(166, 13);
            this.lblAmount.TabIndex = 15;
            this.lblAmount.Text = "Slots to add (min. -127 /max. 127)";
            // 
            // numericSlotNumber
            // 
            this.numericSlotNumber.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.numericSlotNumber.ForeColor = System.Drawing.Color.White;
            this.numericSlotNumber.TextAlign = HorizontalAlignment.Right;
            this.numericSlotNumber.Location = new System.Drawing.Point(13, 21);
            this.numericSlotNumber.Maximum = new decimal(new int[] {
            127,
            0,
            0,
            0});
            this.numericSlotNumber.Minimum = new decimal(new int[] {
            127,
            0,
            0,
            -2147483648});
            this.numericSlotNumber.Name = "numericSlotNumber";
            this.numericSlotNumber.Size = new System.Drawing.Size(59, 20);
            this.numericSlotNumber.TabIndex = 14;
            this.numericSlotNumber.TextChanged += new System.EventHandler(this.numericSlotNumber_UpdateValue);
            this.numericSlotNumber.ValueChanged += new System.EventHandler(this.numericSlotNumber_ValueChanged);
            // 
            // checkBoxEnable
            // 
            this.checkBoxEnable.AutoSize = true;
            this.checkBoxEnable.Checked = true;
            this.checkBoxEnable.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxEnable.Enabled = false;
            this.checkBoxEnable.Location = new System.Drawing.Point(160, 75);
            this.checkBoxEnable.Name = "checkBoxEnable";
            this.checkBoxEnable.Size = new System.Drawing.Size(82, 17);
            this.checkBoxEnable.TabIndex = 13;
            this.checkBoxEnable.Text = "Patchstatus";
            this.checkBoxEnable.UseVisualStyleBackColor = true;
            // 
            // checkBoxTestpatch
            // 
            this.checkBoxTestpatch.AutoSize = true;
            this.checkBoxTestpatch.Location = new System.Drawing.Point(5, 23);
            this.checkBoxTestpatch.Name = "checkBoxTestpatch";
            this.checkBoxTestpatch.Size = new System.Drawing.Size(106, 17);
            this.checkBoxTestpatch.TabIndex = 24;
            this.checkBoxTestpatch.Text = "Latest Testpatch";
            this.checkBoxTestpatch.UseVisualStyleBackColor = true;
            this.checkBoxTestpatch.CheckStateChanged += new System.EventHandler(this.checkBoxTestpatch_CheckStateChanged);
            this.checkBoxTestpatch.Hide();
            // 
            // BackPack
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.ClientSize = new System.Drawing.Size(268, 123);
            this.Controls.Add(this.btnRemPatch);
            this.Controls.Add(this.lblSpaceTrigger);
            this.Controls.Add(this.numericUDTrigger);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.lblTotalCount);
            this.Controls.Add(this.lblTotalSlots);
            this.Controls.Add(this.lblAmount);
            this.Controls.Add(this.numericSlotNumber);
            this.Controls.Add(this.checkBoxEnable);
            this.Controls.Add(this.checkBoxTestpatch);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BackPack";
            this.Text = "BackPack";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BackPack_FormClosing);
            this.Load += new System.EventHandler(this.BackPack_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUDTrigger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericSlotNumber)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRemPatch;
        private System.Windows.Forms.Label lblSpaceTrigger;
        private System.Windows.Forms.NumericUpDown numericUDTrigger;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Label lblTotalCount;
        private System.Windows.Forms.Label lblTotalSlots;
        private System.Windows.Forms.Label lblAmount;
        private System.Windows.Forms.NumericUpDown numericSlotNumber;
        private System.Windows.Forms.CheckBox checkBoxEnable;
        private System.Windows.Forms.CheckBox checkBoxTestpatch;
    }
}