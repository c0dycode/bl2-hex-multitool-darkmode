﻿using System.Drawing;
using System.Windows.Forms;

namespace BL2HexMultitool
{
    partial class MaxLVL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MaxLVL));
            this.btnLVLUndo = new System.Windows.Forms.Button();
            this.btnLVLClose = new System.Windows.Forms.Button();
            this.btnLVLApply = new System.Windows.Forms.Button();
            this.numericUpDownMaxLevel = new System.Windows.Forms.NumericUpDown();
            this.lblMaxLVL = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxLevel)).BeginInit();
            this.SuspendLayout();
            // 
            // btnLVLUndo
            // 
            this.btnLVLUndo.Location = new System.Drawing.Point(177, 22);
            this.btnLVLUndo.Name = "btnLVLUndo";
            this.btnLVLUndo.Size = new System.Drawing.Size(75, 23);
            this.btnLVLUndo.TabIndex = 9;
            this.btnLVLUndo.Text = "Undo";
            this.btnLVLUndo.UseVisualStyleBackColor = true;
            this.btnLVLUndo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnLVLUndo.ForeColor = Color.White;
            this.btnLVLUndo.Click += new System.EventHandler(this.btnLVLUndo_Click);
            // 
            // btnLVLClose
            // 
            this.btnLVLClose.Location = new System.Drawing.Point(256, 22);
            this.btnLVLClose.Name = "btnLVLClose";
            this.btnLVLClose.Size = new System.Drawing.Size(75, 23);
            this.btnLVLClose.TabIndex = 8;
            this.btnLVLClose.Text = "Close";
            this.btnLVLClose.UseVisualStyleBackColor = true;
            this.btnLVLClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnLVLClose.ForeColor = Color.White;
            this.btnLVLClose.Click += new System.EventHandler(this.btnLVLClose_Click);
            // 
            // btnLVLApply
            // 
            this.btnLVLApply.Location = new System.Drawing.Point(97, 22);
            this.btnLVLApply.Name = "btnLVLApply";
            this.btnLVLApply.Size = new System.Drawing.Size(75, 23);
            this.btnLVLApply.TabIndex = 7;
            this.btnLVLApply.Text = "Apply";
            this.btnLVLApply.UseVisualStyleBackColor = true;
            this.btnLVLApply.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnLVLApply.ForeColor = Color.White;
            this.btnLVLApply.Click += new System.EventHandler(this.btnLVLApply_Click);
            // 
            // numericUpDownMaxLevel
            // 
            this.numericUpDownMaxLevel.Location = new System.Drawing.Point(15, 25);
            this.numericUpDownMaxLevel.Maximum = new decimal(new int[] {
            92,
            0,
            0,
            0});
            this.numericUpDownMaxLevel.Minimum = new decimal(new int[] {
            72,
            0,
            0,
            0});
            this.numericUpDownMaxLevel.Name = "numericUpDownMaxLevel";
            this.numericUpDownMaxLevel.Size = new System.Drawing.Size(72, 20);
            this.numericUpDownMaxLevel.TabIndex = 6;
            this.numericUpDownMaxLevel.Value = new decimal(new int[] {
            72,
            0,
            0,
            0});
            this.numericUpDownMaxLevel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.numericUpDownMaxLevel.ForeColor = Color.White;
            this.numericUpDownMaxLevel.TextAlign = HorizontalAlignment.Right;
            this.numericUpDownMaxLevel.ValueChanged += new System.EventHandler(this.numericUpDownMaxLevel_ValueChanged);
            // 
            // lblMaxLVL
            // 
            this.lblMaxLVL.AutoSize = true;
            this.lblMaxLVL.Location = new System.Drawing.Point(12, 9);
            this.lblMaxLVL.Name = "lblMaxLVL";
            this.lblMaxLVL.Size = new System.Drawing.Size(75, 13);
            this.lblMaxLVL.TabIndex = 5;
            this.lblMaxLVL.Text = "Set Max Level";
            // 
            // MaxLVL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.ForeColor = Color.White;
            this.ClientSize = new System.Drawing.Size(343, 51);
            this.Controls.Add(this.btnLVLUndo);
            this.Controls.Add(this.btnLVLClose);
            this.Controls.Add(this.btnLVLApply);
            this.Controls.Add(this.numericUpDownMaxLevel);
            this.Controls.Add(this.lblMaxLVL);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MaxLVL";
            this.Text = "MaxLVL";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MaxLVL_FormClosing);
            this.Load += new System.EventHandler(this.MaxLVL_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxLevel)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLVLUndo;
        private System.Windows.Forms.Button btnLVLClose;
        private System.Windows.Forms.Button btnLVLApply;
        private System.Windows.Forms.NumericUpDown numericUpDownMaxLevel;
        private System.Windows.Forms.Label lblMaxLVL;
    }
}