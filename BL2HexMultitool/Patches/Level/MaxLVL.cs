﻿/*
					Copyright 2017 c0dycode

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Forms;

namespace BL2HexMultitool
{
    public partial class MaxLVL : Form
    {
        
        //public int internalmaxlevel { get; set; }
        public PlayerLevel PLevel = new PlayerLevel();
        public int Templvladdressv1 { get; set; }
        public int Templvladdressv2 { get; set; }
        public int TempEnemyScalingAddress { get; set; }

        public MaxLVL()
        {
            InitializeComponent();
        }

        private void btnLVLClose_Click(object sender, EventArgs e)
        {
            //Properties.Settings.Default.Save();
            this.Close();
        }

        private void MaxLVL_Load(object sender, EventArgs e)
        {

            //int templvladdressv1 = CheckPatched.CheckMaxLvL1(checklvl1);
            //int Templvladdressv2 = CheckPatched.CheckMaxLvL2(checklvl2);

            //Templvladdressv1 = CheckPatched.ScanForPatternAddress(checklvl1, -1);
            //Templvladdressv2 = CheckPatched.ScanForPatternAddress(checklvl2, 11);
            //PlayerLevel.ScalingAddress = CheckPatched.ScanForPatternAddress(checkscaling, -5);
            //TempEnemyScalingAddress = CheckPatched.ScanForPatternAddress(enemyscaling, 11);
            
            var tempList = PLevel.GetLevelAddresses();
            
            Templvladdressv1 = tempList[0] - 1;
            Templvladdressv2 = tempList[1] + 11;
            PlayerLevel.ScalingAddress = tempList[2] - 5;
            TempEnemyScalingAddress = tempList[3] + 11;

            
            if (Templvladdressv1 > 2 && Templvladdressv2 > 2)
            {
                try
                {
                    using (var stream = new FileStream(CheckPatched.BLPath, FileMode.Open, FileAccess.ReadWrite))
                    {
                        
                        // Read default or previously set Level
                        stream.Position = Templvladdressv1;
                        int statusv1 = stream.ReadByte();
                        stream.Position = Templvladdressv2 + 1;
                        int statusv2 = stream.ReadByte();

                        
                        // Default value at the read position is 96
                        // Therefore lvl 72 is max / no patch has been applied yet
                        if (statusv2 == 96)
                        {
                            PlayerLevel.MaxLevel = PLevel.DefaultMaxLevel;
                            numericUpDownMaxLevel.Value = PLevel.DefaultMaxLevel;
                        }
                        // Otherwise the chosen max Level is the read value
                        else
                        {
                            PlayerLevel.MaxLevel = statusv2 + 22;
                            numericUpDownMaxLevel.Value = statusv2 + 22;
                        }
                        
                    }
                }
                // TODO: Modify exceptionhandling to show more accurate messages
                catch (FileNotFoundException)
                {
                    MessageBox.Show("Could not find Borderlands2.exe!");
                    this.Close();
                }
                catch (IOException excep)
                {
                    MessageBox.Show("Could not open Borderlands2.exe. Is it opened by another application?");
                    this.Close();
                }
                catch (Exception excep)
                {
                    MessageBox.Show("Something went wrong!\n {0}", excep.Message);
                    this.Close();
                }
                finally
                {
                    GC.Collect();
                }
            }
        }

        private void btnLVLUndo_Click(object sender, EventArgs e)
        {
            try
            {
                PLevel.RemoveLevelPatch(Templvladdressv1, Templvladdressv2, TempEnemyScalingAddress);
                numericUpDownMaxLevel.Value = PLevel.DefaultMaxLevel;
                //using (var stream = new FileStream(CheckPatched.BLPath, FileMode.Open, FileAccess.ReadWrite))
                //{
                //    byte finalvalue = Convert.ToByte(PLevel.MaxLevel - 22);

                //    // Restoring default Max Level without DLC (50)
                //    stream.Position = Templvladdressv1;
                //    stream.WriteByte(0x32);

                //    // Restoring a functioncall
                //    // This function should return max possible level to gain xp on, IIRC
                //    stream.Position = Templvladdressv2;
                //    stream.WriteByte(0xE8);
                //    stream.Position = Templvladdressv2 + 1;
                //    stream.WriteByte(0x60);
                //    stream.Position = Templvladdressv2 + 2;
                //    stream.WriteByte(0x26);
                //    stream.Position = Templvladdressv2 + 3;
                //    stream.WriteByte(0x1A);
                //    stream.Position = Templvladdressv2 + 4;
                //    stream.WriteByte(0xFF);


                //    //Restore Enemy-Scaling behaviour, so they stop at X
                //    stream.Position = PLevel.ScalingAddress;
                //    stream.WriteByte(0x0B);
                //    stream.Position = TempEnemyScalingAddress;
                //    stream.WriteByte(0x7D);
                //    stream.Position = TempEnemyScalingAddress + 1;
                //    stream.WriteByte(0x02);
                //}

            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("Could not find Borderlands2.exe!");
            }
            catch (IOException excep)
            {
                MessageBox.Show("Could not open Borderlands2.exe. Is it opened by another application?");
            }
            catch (Exception excep)
            {
                MessageBox.Show("Something went wrong!\n{0}", excep.Message);
            }
            finally
            {
                MessageBox.Show("Patch has been reverted!");
                GC.Collect();
            }
        }

        private void numericUpDownMaxLevel_ValueChanged(object sender, EventArgs e)
        {
            int currentValue = Convert.ToInt32(Math.Round(numericUpDownMaxLevel.Value, 0));
            PlayerLevel.MaxLevel = currentValue;
        }

        private void btnLVLApply_Click(object sender, EventArgs e)
        {
            try
            {
                
                PLevel.ApplyLevelPatch(Templvladdressv1, Templvladdressv2, TempEnemyScalingAddress);
                //using (var stream = new FileStream(CheckPatched.BLPath, FileMode.Open, FileAccess.ReadWrite))
                //{

                //    // Hardcoding the default max lvl without DLC
                //    // IIRC it usually is loaded dynamically and would break OP levels if not hardcoded
                //    stream.Position = Templvladdressv1;
                //    stream.WriteByte(0x32);

                //    // Overwriting a functioncall
                //    // This basically increases the base-max-level without DLC (50) to allow you to keep gaining XP
                //    // Calculation = Chosen max Level - 22
                //    byte finalvalue = Convert.ToByte(PLevel.MaxLevel - 22);

                //    stream.Position = Templvladdressv2;
                //    stream.WriteByte(0xB8);
                //    stream.Position = Templvladdressv2 + 1;
                //    stream.WriteByte(finalvalue);
                //    stream.Position = Templvladdressv2 + 2;
                //    stream.WriteByte(0x00);
                //    stream.Position = Templvladdressv2 + 3;
                //    stream.WriteByte(0x00);
                //    stream.Position = Templvladdressv2 + 4;
                //    stream.WriteByte(0x00);

                //    // Calculating the DLC Levelamount for the UVHM DLC
                //    // Calculation = Chosen MaxLevel - 61 (That's 50 baselevel + first

                //    int finalscalingvalue = PLevel.MaxLevel - 72;
                //    byte basescaling = 11;
                //    byte calculatedscaling = Convert.ToByte(PLevel.MaxLevel - 61);
                //    if (finalscalingvalue > 0)
                //    {
                //        basescaling = Convert.ToByte(finalscalingvalue + 11);
                //    }
                //    stream.Position = PLevel.ScalingAddress;
                //    stream.WriteByte(calculatedscaling);

                //    // Disables/Removes a jump that allows enemies to keep scaling with you
                //    stream.Position = TempEnemyScalingAddress;
                //    stream.WriteByte(0x90);
                //    stream.Position = TempEnemyScalingAddress + 1;
                //    stream.WriteByte(0x90);
                //}
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("Could not find Borderlands2.exe!");
            }
            catch (IOException excep)
            {
                MessageBox.Show("Could not open Borderlands2.exe. Is it opened by another application?");
            }
            catch (Exception excep)
            {
                MessageBox.Show("Something went wrong!\n{0}", excep.Message);
            }
            finally
            {
                MessageBox.Show("Patch has been applied!");
                GC.Collect();
            }
        }

        private void MaxLVL_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Properties.Settings.Default.Save();
        }
    }
}
